/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from "react";
import {AppRegistry} from "react-native";
import {StackNavigator} from "react-navigation";
import LandingScreen from "./app/containers/LandingScreen";
import SignupScreen from "./app/containers/SignupScreen";
import SigninScreen from "./app/containers/SigninScreen";
import HomeScreen from "./app/containers/HomeScreen";

const ConsumitNavigation = StackNavigator({
    Landing: {screen: LandingScreen},
    SignUp: {screen: SignupScreen},
    SignIn: {screen: SigninScreen},
    Home: {screen: HomeScreen},
}, {
    mode: 'screen'
});

// const devNav = StackNavigator({
//     DevScreen: {screen: NewChallengeScreen},
//
//     // headerRight: {screen: FriendsScreen}
// });

AppRegistry.registerComponent('consumit', () => ConsumitNavigation);