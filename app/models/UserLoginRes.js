/**
 * Created by lamtran on 8/20/17.
 */
import BaseModel from "./Response";

export default class UserLoginRes extends BaseModel {
    access_token: String;
    email: String;
    id: String;

    // constructor(access_token: String, id: String, email: String) {
    //     super();
    //     this.access_token = access_token;
    //     this.email = email;
    //     this.id = id;
    // }
}
