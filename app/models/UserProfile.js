/**
 * Created by lamtran on 8/20/17.
 */
import BaseModel from "./Response";


export class UserProfile extends BaseModel {
    gender: String;
    date_of_birth: String;
    display_name: String;
    pic: String;

    member_since: String;
    timezone: String;
    id: string;
    email: String;
    age: Number;
}