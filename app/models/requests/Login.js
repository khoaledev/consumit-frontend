/**
 * Created by lamtran on 8/22/17.
 */
import Request from "./Request";
export default class Login extends Request {
    constructor(email: String, password: String) {
        super();
        this.email = email;
        this.password = password;
    }

    email: String;
    password: String;
}