import Request from "./Request"

export default class FoodItem extends Request{
    name: String;
    type: String;
    quantity: Number;
    quantity_type: String;
    exp_days: Number;
}

export class UpdateFoodItem extends Request{
    action: string;
    amount: number;
    message: string;

    constructor(action: string, amount: number, message: string) {
        super();
        this.action = action;
        this.amount = amount;
        this.message = message;
    }
}