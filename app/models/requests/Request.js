/**
 * Created by lamtran on 8/22/17.
 */
export default class Request {
    user_id: String;
    access_token: String;
}