/**
 * Created by lamtran on 8/22/17.
 */
import Request from "./Request";

export default class AuthHeader extends Request {
    user_id: String;
    access_token: String;

    constructor(user_id: String, access_token: String) {
        super();
        this.user_id = user_id;
        this.access_token = access_token;
    }
}