import Request from "./Request";

export class PushNotiToken extends Request{
    token: string;
    os: number;
    constructor(token: string, os: string) {
        super();
        this.token = token;
        this.os = os;
    }
}