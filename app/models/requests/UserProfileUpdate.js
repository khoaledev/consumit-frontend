/**
 * Created by lamtran on 8/20/17.
 */
import Request from "./Request";

export class UserProfileUpdate extends Request {
    gender: String;
    date_of_birth: String;
    display_name: String;
    // pic: String;
    // timezone: String;
}