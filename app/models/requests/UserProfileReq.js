/**
 * Created by lamtran on 8/22/17.
 */
import Request from "./Request";

export default class UserProfileReq extends Request {
    constructor(email: String) {
        super();
        this.email = email;
    }

    email: String;
}