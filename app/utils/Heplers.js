import {AsyncStorage} from "react-native";

/**
 * Function to log in the console for debugging and info
 * @param context: component which calls the log function
 * @param message: the message to be shown
 * @constructor
 */
export function LOG(context, message) {
    if (context && context.constructor)
        console.log([context.constructor.name], " ----- " + message + " ----- ");
    else
        console.log('?', " ----- " + message + " ----- ");
}

/**
 * Saves the component state passed to the function
 * @param key: the key of the state to be saved
 * @param state: current state which is to be saved
 * @returns {Promise<T | void>}
 */
export function setComponentState(key, state) {
    console.log(`saving state of ${key}`, state);
    return AsyncStorage.setItem(key, JSON.stringify(state))
        .then((result) => {
            console.log( `success saving state of ${key}`);
            return result
        })
        .catch(error => console.log(`Fail to save state of ${key} with error ${error}`));

}

/**
 * Getting the stored state using the key
 * @param key: name of the state which needs to be retrieved
 * @returns {Promise<T | void>}
 */
export function getComponentState(key) {
    console.log(`retrieving state of ${key}`);
    return AsyncStorage.getItem(key)
        .then(res => {
            res = JSON.parse(res);
            console.log(`successfully retrieving state of ${key}`, res);
            return res})
        .catch(error => console.log("Fail to get auth user " + error));
}

/**
 * groupby function
 * @param items array of element to groupby
 * @param keyExtractor extract key for each element
 * @returns {*}
 */
export function groupBy(items, keyExtractor){
    return items.reduce(
        (result, item) => {
            let key = keyExtractor(item);
            (result[key] = result[key] || []).push(item);
            return result
        },
        {}
    )
}