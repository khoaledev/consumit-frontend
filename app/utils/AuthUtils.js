import UserLoginRes from "../models/UserLoginRes";
import {AsyncStorage, Platform} from "react-native";
import Constants from "../utils/Constants";
import {LOG} from "./Heplers";
import {PushNotiToken} from "../models/requests/User";
import {getPushToken, setPushToken} from "./Users";
import {removePushToken} from "../services/UserServices";
import BaseContainer from "../containers/BaseContainer";
import {navigationReset} from "./NavigationUtils";


export function setAuthUSer(user: UserLoginRes) {
    try {
        AsyncStorage.setItem(Constants.AUTH_USER, JSON.stringify(user))
            .then((result) => {LOG(this, 'success saving authUser')});
    } catch (error) {
        console.log("Fail to save user with error " + error);
    }
}

export function getAuthUser() {
    try {
        return AsyncStorage.getItem(Constants.AUTH_USER).then(res => JSON.parse(res));
    } catch (error) {
        console.log("Fail to get auth user " + error);
        return null;
    }
}

export function logout(navigation, context: BaseContainer, tag) {
    getPushToken().then(token => {
        let os = Platform.OS;
        removePushToken('me', new PushNotiToken(token, os), context, tag)
    });

}

export function isLoggedIn() {
    return AsyncStorage.getItem(Constants.AUTH_USER);
}