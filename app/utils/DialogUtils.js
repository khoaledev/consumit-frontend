export function showError(title: String, message: String) {
    console.log("Error with message: " + title + " " + message);
}