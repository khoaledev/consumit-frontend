import {NavigationActions} from "react-navigation";


/**
 * a helper to reset navigation to the provided route name, all previous navigation will be cleared
 * @param routeName: route name to navigate to, this will be the only route left in the stack
 * @param navigation: navigation to dispatch
 */
export function navigationReset(routeName, navigation) {
    navigation.navigate(routeName);

    // TODO: reset without causing reload
    // const resetAction = NavigationActions.reset({
    //     index: 0,
    //     key: null,
    //     actions: [
    //         NavigationActions.navigate({routeName: routeName, type: 'Navigation/NAVIGATE'})
    //     ]
    // });
    // navigation.dispatch(resetAction);
}