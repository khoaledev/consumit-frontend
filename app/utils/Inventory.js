import Moment from "moment";
import {AsyncStorage} from "react-native";
import Constants from "./Constants";
import {LOG} from "./Heplers";

//TODO: Decide the categories to display.
export function FoodCategorization(items) {
    let fruit = [];
    let vegetable = [];
    items.forEach((item) => {
        if (item.type === 'fruit')
            fruit.push(item);
        else
            vegetable.push(item);
    });
    return [{title: 'fruit', data: fruit}, {title: 'vegetable', data: vegetable}]
}


