/**
 * Created by khoale on 4/20/18.
 */

import {LOG} from "./Heplers";

/**
 * mimic pub/sub or observer pattern to transmit pub notification data from global cope to components
 */
export class PubSubManager {

    static topics = {};

    /**
     * create a new topic
     * @param topicName
     */
    static createTopic(topicName) {
        let topics = PubSubManager.topics;
        if (!topics[topicName]) {
            LOG(this, `creating topic ${topicName}`);
            topics[topicName] = []
        } else {
            LOG(this, `topic ${topicName} already exists. NOT CREATING NEW`)
        }
    }

    /**
     * remove topic
     * @param topicName
     */
    static removeTopic(topicName: String) {
        LOG(this, `removing topic ${topicName}`);
        delete PubSubManager.topics[topicName];
    }

    /**
     * get all subscriber handlers of a topic, if topic doesn't exists, empty array is returned
     * @param topicName
     * @returns {*|Array}
     */
    static getSubscribers(topicName: String) {
        let topics = PubSubManager.topics;
        if(!topics)
            LOG(this, `topics ${topicName} does not exists or was deleted`);
        return topics[topicName] || []
    }

    /**
     * a subscriber register it's self to the topic by providing the interested topic name and handler function
     * @param topicName
     * @param handler
     * @returns the index of handler
     */
    static addSubscriber(topicName: String, handler: Function) {
        let topics = PubSubManager.topics;
        if (typeof handler !== 'function') {
            console.warn('handler is not a function, subscriber is not registering for topic: ', topicName);
            return -1;
        }
        if (!topics[topicName]) {
            console.warn(topicName, ' topic does not exist, not registering');
            return -1;
        }
        let length = topics[topicName].push(handler);
        return length - 1;
    }

    /**
     * when data is send to a topic, all subscribers' handlers will be called with data passed in
     * @param topicName
     * @param data: data to pass in handler
     * @returns number of subcribers in the topic, 0 means no subscribers
     */
    static sendDataToTopic(topicName: String, data) {
        LOG(this, `send data to topic ${topicName}`);
        let subscribers = this.getSubscribers(topicName);
        for (let i=0; i < subscribers.length; i++) {
            let handler = subscribers[i];
            handler(data);
        }
        return subscribers.length;
    }

    /**
     * remove a subscriber handler by index
     * @param topicName
     * @param handler
     * @returns {boolean}
     */
    static removeSubscriber(topicName: String, handler: Function){
        LOG(this, `remove a subscriber's handler of topic ${topicName}`);
        let subscribers = this.getSubscribers(topicName);

        let index = subscribers.indexOf(handler);
        if(index >= 0){
            subscribers.splice(index, 1);
            return true;
        }
        LOG(this, `subscriber does not exists in topic ${topicName} or topic does not exists`, handler);
        return false
    }
}
