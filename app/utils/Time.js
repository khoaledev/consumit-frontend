import Moment from "moment";

/**
 * function returning timestamps of today and the date counting from today a number of days.
 * @param days: number of days from now
 * @param future: whether to count the days to the future, false is counting to the past, default to false
 * @returns {{start: number, end: number}}
 */
export function fromToday(days=7, future=false){
    days = Math.abs(days);
    let past = Moment().startOf('day').utc().subtract(days, 'days').valueOf()/1000;
    let today = Moment().startOf('day').utc().valueOf()/1000;
    let ft = Moment().startOf('day').utc().add(days, 'days').valueOf()/1000;
    if (future)
        return {start: today, end: ft};
    return {start: past, end: today};
}

export function compute_days_diff(start, end){
    let s = Moment(start);
    let e = Moment(end);
    return e.diff(s, 'days');
}