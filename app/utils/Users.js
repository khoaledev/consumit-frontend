import UserLoginRes from "../models/UserLoginRes";
import {AsyncStorage} from "react-native";
import Constants from "../utils/Constants";
import {LOG} from "./Heplers";

/**
 * set whether push token is registered to the backend or not
 * @param reg
 * @returns {Promise<*>|Promise<Boolean>}
 */
export function setPushTokenRegis(reg: Boolean) {
    try {
        return AsyncStorage.setItem(Constants.PUSH_TOKEN_REGIS, JSON.stringify(reg))
            .then((result) => {
                LOG(this, 'Success in saving push notification registration to local', result);
                return reg
            });
    } catch (error) {
        console.log("Fail to save push notification registration to local with error " + error);
    }
}

/**
 * read to see if push token is registered to the backend or not
 * @returns {*}
 */
export function getPushTokenRegis() {
    try {
        return AsyncStorage.getItem(Constants.PUSH_TOKEN_REGIS).then(res => JSON.parse(res));
    } catch (error) {
        console.log("Fail to get getPushNotiRegis to local: " + error);
        return null;
    }
}

/**
 * store push token locally
 * @param token
 * @returns {Promise<*>|Promise<T>}
 */
export function setPushToken(token: string) {
    try {
        LOG(this, 'Storing Push Token');
        return AsyncStorage.setItem(Constants.PUSH_TOKEN, JSON.stringify(token))
            .then((result) => {
                LOG(this, 'success saving push token', result);
                return result
            });
    } catch (error) {
        console.log("Fail to save push token with error " + error);
    }
}

/**
 * retrieve push token from local
 * @returns {*}
 */
export function getPushToken() {
    try {
        LOG(this, 'Trying to retrieve Local Push Token');
        return AsyncStorage.getItem(Constants.PUSH_TOKEN).then(res => JSON.parse(res));
    } catch (error) {
        console.log("Fail to get get Push Token " + error);
        return null;
    }
}

/**
 * store push notification locally
 * @param push_noti
 */
export function setPushNoti(push_noti) {
    try {
        LOG(this, `Saving Push Notification with ${JSON.stringify(push_noti)}`);
        AsyncStorage.setItem(Constants.PUSH_NOTI, JSON.stringify(push_noti))
            .then((result) => {
                LOG(this, 'success saving push notification', result)
            });
    } catch (error) {
        console.log("Fail to save push token with error " + error);
    }
}

/**
 * retrieve push notification from local
 * @returns {*}
 */
export function getPushNoti() {
    try {
        LOG(this, 'trying to retrieving Push notification');
        return AsyncStorage.getItem(Constants.PUSH_NOTI).then(res => JSON.parse(res));
    } catch (error) {
        console.log("Fail to get get Push Token " + error);
        return null;
    }
}
