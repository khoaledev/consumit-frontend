import CustomPicker from "react-native-picker/index";

export const TYPE = {
    Fruit: 'fruit',
    Vegetable: 'vegetable',
    Dairy: 'dairy',
    Meat: 'meat',
    Grain: 'grain',
    Legume: 'legume'
};

export const Fruits = ['Apple', 'Avocado', 'Banana', 'Blackberries', 'Blueberries', 'Grapefruit',
    'Grapes', 'Lemon', 'Lime', 'Mango', 'Nectarine', 'Orange', 'Papaya', 'Peach', 'Pear', 'Pineapple',
    'Plum', 'Raspberries', 'Strawberries', 'Watermelon'];
export const Vegetables = ['Asparagus','Beet','Bell Pepper','Broccoli','Cabbage','Carrot','Cauliflower',
    'Celery','Cucumber','Garlic','Green Bean','Green Onion','Lettuce','Mushroom','Onion','Potato','Spinach',
    'Sweet Potato','Tomato','Zucchini'];
export const Dairies = ['Butter','Cheese','Cream','Ice Cream','Margarine','Milk','Paneer','Sour Cream',
    'Whipped Cream','Yogurt'];
export const Meats = ['Bacon','Beef','Chicken','Eggs','Lamb','Pork','Salami','Shrimp','Sausage','Turkey'];
export const Grains = ['Barley','Maize','Oat','Wheat','Rice'];
export const Legumes = ['Beans','Chickpeas','Lentils','Peas','Soybean'];

export function getType(name) {
    let type = '';
    if (Fruits.includes(name)) {
        type = TYPE.Fruit;
    }

    if (Vegetables.includes(name)) {
        type = TYPE.Vegetable;
    }

    if (Dairies.includes(name)) {
        type = TYPE.Dairy;
    }

    if (Meats.includes(name)) {
        type = TYPE.Meat;
    }

    if (Grains.includes(name)) {
        type = TYPE.Grain;
    }

    if (Legumes.includes(name)) {
        type = TYPE.Legume;
    }

    return type;
}

/**
 * get the date apart from now given the number of days
 * @param days
 * @returns {Date}
 * @private
 */
export function getDateFromDays(days) {
    if (!days)
        days = 0;
    let date = new Date();
    date.setDate(date.getDate() + parseInt(days));
    return date;
}

export function getDaysFromType(type) {
    let days = '0';
    if (type) {
        if (type === TYPE.Fruit) {
            days = '10';
        } else if (type === TYPE.Vegetable) {
            days = '7';
        } else if (type === TYPE.Vegetable) {
            days = '7';
        } else if (type === TYPE.Dairy) {
            days = '12';
        } else if (type === TYPE.Meat) {
            days = '15';
        } else if (type === TYPE.Grain) {
            days = '30';
        } else if (type === TYPE.Legume) {
            days = '60';
        }
    }
    return days;
}

/**
 * get number of days apart from now given a date
 * @param date
 * @returns {number}
 * @private
 */
export function getDaysFromDate(date) {
    let today = new Date();
    let days = date - today;
    days = Math.ceil(days / (1000 * 3600 * 24));
    return days;
}

/**
 * helper function for _initCustomPicker()
 * @param type : type of data state
 * @returns {*} : array of element
 * @private
 */
export function getDataByType(type) {
    switch (type.toLowerCase()) {
        case "type":
            return ['fruit', 'vegetable', 'diary', 'canned'];
        case "quantity type":
            return ['count', 'lb', 'oz', 'fl oz', 'gal'];
        default:
            return [];
    }
}

/**
 * custom picker (string, number)
 * @param type : type of picker, type is determined in ) getDataByType.
 * @param selectedValue : initialize selected value when picker open.
 * @param onPickerConfirm : callback function when picker is confirmed.
 * @private
 */
export function initCustomPicker({type, selectedValue, onPickerConfirm}) {
    let pickerData = getDataByType(type);
    CustomPicker.init({
        pickerTitleText: type,
        pickerData: pickerData,
        selectedValue: selectedValue,
        onPickerConfirm: onPickerConfirm,
        onPickerCancel: data => {
        },
        onPickerSelect: data => {
        },
    });
    CustomPicker.show()
}