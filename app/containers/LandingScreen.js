/**
 * Created by khoale on 4/13/2018.
 */
import React from "react";
import {Image, StatusBar, View} from "react-native";
import styles from "../Styles";
import {getAuthUser} from "../utils/AuthUtils";
import {LOG} from "../utils/Heplers";
import {TextButton} from "../components/TextButton";
import BaseContainer from "./BaseContainer";
import {navigationReset} from "../utils/NavigationUtils";


export default class LandingScreen extends BaseContainer {
    static navigationOptions = {title: 'Welcome', header: null};

    constructor(props) {
        super(props);
        this.state = {show: false}
    }

    componentDidMount() {
        // super.componentDidMount();
        LOG(this, 'component did mount');

        LOG(this, 'checking valid user');
        getAuthUser().then(res => {
            if (res) {
                // getUserProfile('me', this, 'profile');
                LOG(this, 'user authentication found, reset navigation to home screen');
                navigationReset("Home", this.props.navigation);
            }
            else {
                LOG(this, 'no user is stored, enabling SIGNUP/SIGNIN');
                this.setState({show: true});
            }
        })


    }

    onSuccess(res, tag) {
        super.onSuccess(res, tag);
    }

    onError(error, tag) {
        super.onError(error, tag);
    }

    _renderBut(text, onPress) {
        if (this.state.show) {
            return (
                <TextButton onPress={onPress} size={"medium"} text={text}/>
            )
        }
    }

    render() {
        return (
            <View style={{
                flex: 1,
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: '#5DC9FF',
            }}>

                <StatusBar hidden={true}/>

                {/*top spacing*/}
                <View style={{flex: 1}}/>

                {/*Logo*/}
                <View style={styles.welcomeLogoContainer}>
                    <Image source={require('../assets/Images/logo_96X96.png')}/>
                    <Image source={require('../assets/Images/consumitname.png')} style={{height: 70}}
                           resizeMode={'contain'}/>
                </View>

                {/*sign up/in btn container*/}
                <View style={styles.signUpInBtnsContainer}>
                    {this._renderBut('Sign In', () => this.props.navigation.navigate('SignIn'))}
                    {this._renderBut('Sign Up', () => this.props.navigation.navigate('SignUp'))}
                </View>

            </View>
        );
    }
}
