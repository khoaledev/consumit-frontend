import React from "react";
import {View, Text, FlatList} from "react-native";
import {Content} from 'native-base';
import {TabNavigator} from "react-navigation";
import BaseContainer from "./BaseContainer";
import {AddItemRow} from "../components/AddItemRow";
import constants from "../utils/Constants";
import {PubSubManager} from "../utils/PubSubManager";

const TYPE = {
    Fruit: 'fruit',
    Vegetable: 'vegetable'
};

export class ShoppingList extends BaseContainer {

    itemId = 0;
    static navigationOptions = ({navigation, screenProps}) => ({
        tabBarLabel: "Shopping List",
    });

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            loadUserAll: false,
            loadUserData: false,
            loadGlobalData: false,
            user_data: [],
            global_data: [],
            data: {
                "shopping_list": [],
                "suggestive_shopping_list": [
                    "Onion",
                    "Orange"
                ]
            }
        };
    }

    componentWillMount() {
        // this.fetchData()
        PubSubManager.createTopic(constants.ADD_ITEM_SAVE);
        let data = this.state.data;
        this.state.data.suggestive_shopping_list.map((name) => {
            let type = this._getType(name);
            let item = this._getNewItem(type);
            item.name = name;
            data.shopping_list.push(item);
        });
        this.setState({data:data});
    }

    componentWillUnmount() {
        PubSubManager.removeTopic(constants.ADD_ITEM_SAVE);
    }

    _getType(name) {
        let type = '';
        if (Fruits.includes(name)) {
            type = TYPE.Fruit;
        }

        if (Vegetables.includes(name)) {
            type = TYPE.Vegetable;
        }

        return type;
    }

    refreshData() {
        // this.setLoading(true);
        // this.setState({
        //     loadUserAll: false,
        //     loadUserData: false,
        //     loadGlobalData: false,
        //     user_data: [],
        //     global_data: [],
        //     user_all: {}
        // });
        // this.fetchData();
    }

    fetchData() {
        // let time = fromToday(100);
        // getFoodPrint(time.start, 0, 'monthly', 'user_all', this, 'user_all');
        // getFoodPrint(time.start, 0, 'monthly', 'user', this, 'user');
        // getFoodPrint(time.start, 0, 'monthly', 'global', this, 'global');

    }

    onSuccess(res, tag) {
        // if (tag === "global")
        //     this.setState({global_data: res, loadGlobalData: true}, this.checkLoadAll);
        // else if (tag === "user")
        //     this.setState({user_data: res, loadUserData: true}, this.checkLoadAll);
        // else if (tag === "user_all")
        //     this.setState({user_all: res, loadUserAll: true}, this.checkLoadAll);
    }

    checkLoadAll() {
        // if (this.state.loadUserAll && this.state.loadUserData && this.state.loadGlobalData)
        //     this.setLoading(false);
    }

    _getId() {
        return this.itemId++
    }

    _getNewItem(type) {
        let id = this._getId();
        let item = {
            id: id,
            days: '',
        };

        if (type === TYPE.Fruit) {
            item.type = [TYPE.Fruit];
            item.days = '10';
        } else if (type === TYPE.Vegetable) {
            item.type = [TYPE.Vegetable];
            item.days = '7';
        }

        return item;
    }

    _addNewRowItem(type) {
        let data = this.state.data;
        let item = '';

        if (type === TYPE.Fruit) {
            item = this.getNewItem(TYPE.Fruit)
        }
        if (type === TYPE.Vegetable) {
            item = this.getNewItem(TYPE.Vegetable)
        }
        data['shopping_list'].push(item);
        this.setState({data: data});
    }

    _renderItem = ({item, index}) => {
        return <AddItemRow item={item}
                           index={index}
                           isShopping={true}/>
    };


    render() {
        return (
            <Content refreshControl={this.showSpinner()}>
                <View>
                    <FlatList
                        refreshControl={this.showSpinner()}
                        data={this.state.data.shopping_list}
                        style={{backgroundColor: 'white'}}
                        keyExtractor={(item, index) => index}
                        renderItem={this._renderItem}
                        // onEndReachedThreshold={0.7}
                        // onEndReached={() => {
                        //     this.getNextPage();
                        // }}
                    />
                </View>
            </Content>
        );
    }
}

export const ShoppingNavigator = TabNavigator({
    ShoppingList: {screen: ShoppingList},
}, {
    tabBarOptions: {
        activeTintColor: 'black',
        inactiveTintColor: 'grey',
        labelStyle: {
            // color: 'black',
            fontSize: 12,
        },
        style: {
            backgroundColor: 'white',
        },
        tabStyle: {
            height: 40,
        }
    }
});