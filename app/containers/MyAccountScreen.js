/**
 * Created by lamtran on 8/11/17.
 */
import React from "react";
import {DatePickerIOS, Text, View} from "react-native";
import {Body, Button, Container, Content, Icon, Input, List, ListItem, Right} from "native-base";
import BaseContainer from "./BaseContainer";
import {getAuthUser} from "../utils/AuthUtils";
import {LOG} from "../utils/Heplers";
import {getUserProfile} from "../services/UserServices";

export default class MyAccountScreen extends BaseContainer {

    static navigationOptions = ({navigation, screenProps}) => {
        const params = navigation.state.params || {};

        return {
            title: "My Account",
            headerRight: (
                <Button transparent primary onPress={() => params.onHeaderRightClicked()}><Text>Edit</Text></Button>)
        }
    };

    constructor(props) {
        super(props);
        let timeZoneOffsetInHours = (-1) * (new Date()).getTimezoneOffset() / 60;

        this.state = {
            date: new Date(),
            showPicker: false,
            timeZoneOffsetInHours: this.props.timeZoneOffsetInHours,
            email: '',
            age: 0,
            gender: 'unknown',
            display_name: '',
            date_of_birth: '',
            member_since:''
        };
    }

    onHeaderRightClicked() {
        super.onHeaderRightClicked();
        let userProfile = {
            email: this.state.email,
            age: this.state.age,
            gender: this.state.gender,
            display_name: this.state.display_name,
            date_of_birth: this.state.date_of_birth,
            member_since: this.state.member_since
        };

        this.props.navigation.navigate('EditMyAccount', {profile: userProfile});
        LOG(this, 'Navigate to EditMyAccount with profile: ' + JSON.stringify(userProfile));
    }

    componentDidMount() {
        super.componentDidMount();
        this.loadData();
    }

    onSuccess(profile, tag) {
        LOG(this, JSON.stringify(profile));
        // let profile = userProfile[0];
        this.setState({
            email: profile.email,
            age: profile.age,
            gender: profile.gender,
            display_name: profile.display_name,
            date_of_birth: profile.date_of_birth,
            member_since: profile.member_since,
            profile_pic: profile.profile_pic
        })
    }

    loadData() {
        this.setLoading(true);
        // getAuthUser().then(authUser => {
        //     LOG(this, 'logged in user: ' + JSON.stringify(authUser));
        //     let reqHeader = new AuthHeader(authUser._id, authUser.access_token);
        //     let url = 'users/' + authUser._id;
        //     sendRequestWithHeader(reqHeader, 'GET', url, null, UserProfile, this);
        // });
        getAuthUser().then(res => {
            if (res) {
                getUserProfile([res._id], this);
            }
        });
    }

    render() {
        return (
            <Container>
                <Content>
                    <List>
                        <ListItem icon>
                            <Body>
                            <Text>Display Name</Text>
                            </Body>
                            <Text style={{textAlign: 'right'}}>{this.state.display_name}</Text>
                        </ListItem>

                        <ListItem icon>
                            <Body>
                            <Text>Age</Text>
                            </Body>
                            <Text style={{textAlign: 'right'}}>{this.state.age}</Text>
                        </ListItem>

                        <ListItem icon>
                            <Body>
                            <Text>Day of birth</Text>
                            </Body>
                            <Right>
                                <Text style={{textAlign: 'right'}}>{this.state.date_of_birth}</Text>
                            </Right>
                        </ListItem>

                        <ListItem icon>
                            <Body>
                            <Text>Gender</Text>
                            </Body>
                            <Right>
                                <Text style={{textAlign: 'right'}}>{this.state.gender}</Text>
                            </Right>
                        </ListItem>
                    </List>
                </Content>
            </Container>
        )
    }
}