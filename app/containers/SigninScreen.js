/**
 * Created by khoale on 8/6/2017.
 */
import React from "react";
import {Button, Container, Content, Form, Input, Item, Label, Text} from "native-base";
import {Image, Alert} from "react-native";
import {sendRequest} from "../services/BaseServices";
import Login from "../models/requests/Login";
import BaseContainer from "./BaseContainer";
import UserLoginRes from "../models/UserLoginRes";
import {setAuthUSer} from "../utils/AuthUtils";
import {TextButton} from "../components/TextButton";
import {SecureInput} from "../components/SecureInput";
import {NavigationActions} from "react-navigation";
import {LOG} from "../utils/Heplers";
import {navigationReset} from "../utils/NavigationUtils";
import {signin} from "../services/UserServices";


export default class SigninScreen extends BaseContainer {
    static navigationOptions = ({navigation}) => ({
        headerTitle: 'Sign Into Consumit',
        headerTintColor: 'white',
        headerStyle: {backgroundColor: '#5DC9FF'}
    });

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            // username: 'khoa@consumit.com',
            // password: 'P@ssw0rd'
            username: '',
            password: ''
        };

        this.login = this.login.bind(this);
    }

    onSuccess(res, tag) {
        // LOG(this, 'enter onSuccess');

        super.onSuccess(res, tag);
        LOG(this, `saving user authen to local`);
        setAuthUSer(res);

        navigationReset("Home", this.props.navigation);
        this.setLoading(false);

    }

    onError(res, tag) {
        LOG(this, 'Enter onError');

        if (res.status === 404) {
            Alert.alert(
                '',
                `${this.state.username} not found`,
                [
                    {text: 'Sign Up', onPress: () => this.props.navigation.navigate("SignUp")},
                    {text: 'Retry', onPress: () => console.log("Retry")}
                ]);
        } else if (res.status === 401) {
            Alert.alert(
                '',
                'Wrong password',
                [
                    {text: 'Forget password', onPress: () => console.log("forget password")},
                    {text: 'Retry', onPress: () => console.log("Retry")}
                ]);
        } else if (res.status === 400) {
            Alert.alert(
                '',
                'invalid password or email')
        }
        this.setLoading(false);
    }

    login() {
        this.setLoading(true);
        signin(new Login(this.state.username, this.state.password), this, 'signin');
    }

    render() {
        return (
            <Container refreshControll={this.showSpinner()}>
                <Content style={{backgroundColor: 'white', paddingTop: 50}}>
                    <Image source={require('../assets/Images/logo.png')} style={{alignSelf: "center"}}/>

                    <Item stackedLabel style={{height:70}}>
                        <Label>Email</Label>
                        <Input numberOfLines={1} value={this.state.username}
                               onChangeText={(text) => this.setState({username: text})}/>
                    </Item>

                    <Item stackedLabel last style={{height:70}}>
                        <Label>Password</Label>
                        <SecureInput value={this.state.password} style={{}}
                                     onChangeText={(text) => this.setState({password: text})}/>
                        {/*<Input secureTextEntry={true} numberOfLines={1} value={this.state.password}*/}
                               {/*onChangeText={(text) => this.setState({password: text})}/>*/}
                    </Item>
                    <TextButton onPress={() => this.login()} style={{alignSelf: "center"}}
                                size={"medium"} text={"Sign In"}/>
                </Content>
            </Container>
        );
    }
}
