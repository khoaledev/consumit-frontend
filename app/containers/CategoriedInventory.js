import React, {Component} from "react";
import {Button, DatePickerAndroid, FlatList, SectionList} from "react-native";
import {
    Input,
    Item,
    Label,
    Container,
    Content,
    Text,
    List,
    ListItem,
    Left,
    Right,
    Body,
    Icon,
    CardItem,
    Header,
    Separator
} from "native-base";
import {ItemsListCell} from "../components/ItemsListCell";
import BaseContainer from "./BaseContainer";
import {fromToday} from "../utils/Time";
import {FoodCategorization} from "../utils/Inventory";
import {getFoodExpiry} from "../services/FoodItemsServices";
import styles from "../Styles";


export class CategorizedInventory extends BaseContainer {
    static navigationOptions = ({navigation, screenProps}) => ({
        title: "Categorized Inventory",
        tabBarLabel: "Categorized",
        header: null
    });

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            inventory: [],
        }
    }

    componentDidMount() {
        this.fetchData()
    }

    refreshData() {
        super.refreshData();
        this.setState({inventory: []});
        this.setLoading(true);
        this.fetchData();
    }

    fetchData() {
        let time = fromToday(15, true);
        getFoodExpiry(time.start, 0, 0, 0, 'expired', '', '', this);
    }

    onSuccess(res, tag) {
        let inventory = FoodCategorization(res);
        this.setState({inventory});
        this.setLoading(false);
    }

    _renderItem = ({item}) => {
        return (
            <ItemsListCell item={item} navigation={this.props.navigation}/>)
    };

    render() {
        return (
            <Content refreshControl={this.showSpinner()} style={styles.pageContainer}>
                <SectionList
                    renderItem={this._renderItem}
                    renderSectionHeader={({section: {title}}) =>
                        <ListItem bordered>
                            <Text style={{fontSize: 15}}>{title}</Text>
                        </ListItem>}
                    sections={this.state.inventory}
                    keyExtractor={(item, index) => item._id}/>
            </Content>

        );
    }
}


