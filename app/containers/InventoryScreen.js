import React from "react";
import {TabNavigator} from "react-navigation"
import {FlatList, View} from "react-native";
import {Button, Icon, Text} from "native-base";
import {ItemsListCell} from "../components/ItemsListCell";
import {ExpiredInventory} from "./ExpiredInventoryScreen";
import {fromToday} from "../utils/Time";
import Moment from "moment";
import {getFoodExpiry} from "../services/FoodItemsServices";
import {LOG} from "../utils/Heplers";
import {RecentlyAddedInventory} from "./RecentlyAddedInventory";
import constants from "../utils/Constants";
import StatefulBaseContainer from "./StatefulBaseContainer";
import {PubSubManager} from "../utils/PubSubManager";
import {Dairies, Fruits, Grains, Legumes, Meats, Vegetables} from "../utils/FoodItem";
import AutoSuggest from "../components/AutoSuggest";

export class CurrentInventory extends StatefulBaseContainer {
    static navigationOptions = ({navigation, screenProps}) => ({
        tabBarLabel: "Current Inventory",
        header: null,
    });

    constructor(props) {
        super(props);
        this._filterFood = this._filterFood.bind(this);
        this._updateItem = this._updateItem.bind(this);
        this._renderFilter = this._renderFilter.bind(this);
        this.state = {
            stateStorageKey: constants.CUR_INV_STATE,
            delayPerPull: 10,
            page: 0,
            size: 20,
            pageEnded: false,
            isLoading: true,
            showToast: false,
            inventory: [],
            foodType: '',
            foodName: ''
        }
    }

    refreshData() {
        super.refreshData();
        this.setLoading(true);

        this.setState(
            {inventory: [], page: 0, pageEnded: false},
            this.fetchData);
    }

    componentDidMount() {
        PubSubManager.createTopic(constants.INVENTORY_ITEM_COLLAPSE);
    }

    componentWillUnmount() {
        PubSubManager.removeTopic(constants.INVENTORY_ITEM_COLLAPSE);
    }

    getNextPage() {
        if (!this.state.pageEnded) {
            ++this.state.page;
            this.fetchData();
        }
    }

    fetchData(page = this.state.page, size = this.state.size) {
        getFoodExpiry(0, 0, page, size, 'expired', this.state.foodName, this.state.foodType, this, null)
    }

    onSuccess(res, tag) {
        // sort by exp days ascendingly
        res.sort((a, b) => Moment(a.exp_date) - Moment(b.exp_date));
        if (res.length < this.state.size) {
            this.state.pageEnded = true;
        }
        let inventory = this.state.inventory;
        inventory.push(...res);
        this.setState({inventory});
        this.setLoading(false);
        this.updateLocalState(true);
    }

    _renderItem = ({item, index}) => {
        return <ItemsListCell
            item={item} index={index}
            filterFood={this._filterFood}
            updateItem={this._updateItem}
        />
    };

    _filterFood({name, type}) {
        if (type === 'all' && this.state.foodType === '')
            return;

        if (name === undefined || this.state.foodName === name)
            this.state.foodName = '';
        else
            this.state.foodName = name;

        if (type === undefined || type === 'all' || this.state.foodType === type)
            this.state.foodType = '';
        else
            this.state.foodType = type;
        this.refreshData()
    }

    _onSelectSuggestion(value) {
        this._filterFood({name: value.toLowerCase()});
    }

    _updateItem({index, item}) {
        if (item.quantity === 0) {
            // re-render if it is consumed
            this.state.inventory.splice(index, 1);
            this.setState({inventory: this.state.inventory});
        } else {
            // dont call setState to prevent re-render the whole list
            this.state.inventory.splice(index, 1, item);
        }
        this.updateLocalState();
    }

    _renderFilter() {
        if (!this.state.foodName)
            return null;

        return (
            <View style={{flexDirection: 'row', padding: 5, alignItems: 'center'}}>
                <Text style={{color: '#5C5C5C', fontSize: 14}}> Filter </Text>
                <Button rounded small title={this.state.foodName} iconRight
                        onPress={() => this._onSelectSuggestion('')}
                        style={{backgroundColor: '#FFF', margin: 1}}>
                    <Text style={{fontSize: 12, color: '#FF7658'}}>
                        {this.state.foodName}
                    </Text>
                    <Icon name='close' style={{fontSize: 15, color: '#FF7658'}}/>
                </Button>
            </View>
        )
    }

    render() {
        return (
            <View style={{flex: 1, backgroundColor: '#F5F5F5'}}>
                <AutoSuggest suggestions={[...Fruits, ...Vegetables, ...Dairies, ...Meats, ...Grains, ...Legumes]}
                             onSelectSuggestion={this._onSelectSuggestion.bind(this)}
                             place_holder={'Search to filter by name'}
                             resetInputOnSelect/>
                <View style={{flex: 1, justifyContent: 'center', marginTop: 55}}>
                    {this._renderFilter()}

                    {/*list item*/}
                    <FlatList
                        refreshControl={this.showSpinner()}
                        data={this.state.inventory}
                        keyExtractor={(item) => item._id}
                        renderItem={this._renderItem}
                        onEndReachedThreshold={0.6}
                        onEndReached={() => {
                            this.getNextPage();
                        }}
                    />
                </View>
            </View>
        )
    }
}

export const InventoryTabNavigator = TabNavigator({
    CurrentInventory: {screen: CurrentInventory},
    RecentlyAddedInventory: {screen: RecentlyAddedInventory},
    ExpiredInventory: {screen: ExpiredInventory},
}, {
    tabBarOptions: {
        activeTintColor: '#0098cc',
        inactiveTintColor: '#5DC9FF',
        labelStyle: {
            fontSize: 12,
        },
        style: {
            backgroundColor: 'white',
        },
        tabStyle: {
            height: 45,
        },
        indicatorStyle: {
            backgroundColor: '#0098cc'
        }
    }
    // navigationOptions: ({navigation, screenProps}) => {
    //     console.log(navigation);
    //     console.log(screenProps);
    //     let a = screenProps['a'];
    //     console.log('inventory navigaion option: ', a);
    //     if(a) {
    //         delete screenProps['a'];
    //         navigation.navigate(a)
    //     }
    //     // navigation.dispatch("ExpiredInventory");
    // }
});

