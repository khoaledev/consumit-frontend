import React from "react";
import {FlatList} from "react-native";
import {Content} from "native-base";
import {ItemsListCell} from "../components/ItemsListCell";
import {fromToday} from "../utils/Time";
import {getFoodExpiry} from "../services/FoodItemsServices";
import constants from "../utils/Constants";
import Moment from "moment/moment";
import StatefulBaseContainer from "./StatefulBaseContainer";
import styles from "../Styles"


export class ExpiredInventory extends StatefulBaseContainer {
    static navigationOptions = ({navigation, screenProps}) => ({
        tabBarLabel: "Recently Expired",
        header: null,
    });

    constructor(props) {
        super(props);
        this.state = {
            stateStorageKey: constants.RECENTLY_EXPIRED_INV_STATE,
            delayPerPull: 10,
            isLoading: true,
            expired: true,
            inventory: []
        }
    }

    fetchData() {
        let time = fromToday(7);
        getFoodExpiry(time.start, time.end, 0, 0, 'expired', '', '', this)
    }

    refreshData() {
        super.refreshData();
        this.setLoading(true);
        this.setState({inventory: []});
        this.fetchData();
    }

    onSuccess(res, tag) {
        let lastPull = Moment();
        this.setState({inventory: res});
        this.setLoading(false);
        this.updateLocalState(true);
    }

    _renderItem = ({item}) => {
        return (
            <ItemsListCell item={item} navigation={this.props.navigation} expired={true}/>)
    };

    render() {
        return (
            <Content refreshControl={this.showSpinner()} style={styles.pageContainer}>
                <FlatList
                    data={this.state.inventory}
                    style={{backgroundColor: '#ECECEC'}}
                    keyExtractor={(item, index) => item._id}
                    renderItem={this._renderItem}/>
            </Content>
        );
    }
}

