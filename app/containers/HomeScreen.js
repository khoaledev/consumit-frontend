/**
 * Created by Khoa on 10/7/2017
 */
import React from "react";
import {Container, Footer, FooterTab, Button, Icon} from 'native-base';
import {SettingNavigator} from "./SettingsScreen";
import {InventoryTabNavigator} from "./InventoryScreen";
import {StatisticNavigator} from "./StatisticScreen";
import BaseContainer from "./BaseContainer";
import {ShoppingNavigator} from "./ShoppingScreen";
import {StyleSheet, Platform, StatusBar} from "react-native";
import {PushNotiToken} from "../models/requests/User";
import Amplify from 'aws-amplify';
import {PushNotification} from 'aws-amplify-react-native';
import aws_exports from '../../aws-exports';
import {getPushNoti, getPushTokenRegis, getPushToken, setPushNoti, setPushTokenRegis, setPushToken} from "../utils/Users";
import {LOG} from "../utils/Heplers";
import {registerPushToken} from "../services/UserServices";
import {PubSubManager} from "../utils/PubSubManager";
import LogoTitle from "../components/LogoTitle";import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);

// PushNotification need to work with Analytics
Amplify.configure(aws_exports);

PushNotification.configure(aws_exports);

const TAB_INVENTORY = 'tabInventory';
const TAB_STATISTIC = 'tabStatistic';
const TAB_SHOPPING = 'tabShopping';
const TAB_SETTING = 'tabSetting';

const PUSH_NOTI_TOPIC = 'push_notification';

PubSubManager.createTopic(PUSH_NOTI_TOPIC);


// get the notification data
PushNotification.onNotification((notification) => {
    // Note that the notification object structure is different from Android and IOS
    console.log('global notification', notification);

    let count = PubSubManager.sendDataToTopic(PUSH_NOTI_TOPIC, notification);

    // if there is no subscribers atm, meaning the app is offline > store the latest notification
    if (count === 0) {
        LOG(this, 'No registered subscribers, saving notification to local');
        setPushNoti(notification);
    } else {
        LOG(this, `Notification has been sent to ${count} subscribers`)
    }

    // required on iOS only (see fetchCompletionHandler docs: https://facebook.github.io/react-native/docs/pushnotificationios.html)
    // notification.finish(PushNotificationIOS.FetchResult.NoData);
});

PushNotification.onRegister((token) => {
    console.log('in app noti token: ', token);
    // check if the token is stored, if not store
    LOG(this, 'check if Token is stored');
    getPushToken().then(res => {
        if (!res) {
            LOG(this, 'No Push Token is in Local Storage');
            setPushToken(token);
        } else {
            LOG(this, 'current stored Push Token is ' + res);
        }
    });
});



export default class HomeScreen extends BaseContainer {
    constructor(props) {
        super(props);
        this.handleNotification = this.handleNotification.bind(this);
        this.setTab = this.setTab.bind(this);
        this.state = {selectedTab: TAB_INVENTORY};
    }

    static navigationOptions = ({navigation, screenProps}) => {
        const params = navigation.state.params || {};
        return {
            // title: "CONSUMIT",
            headerTitle: <LogoTitle/>,
            headerTintColor: 'white',
            headerStyle: {backgroundColor: '#5DC9FF'}
            // headerBackground: '#6AD4E8'
        }
    };

    handleNotification(notification) {
        if (notification.foreground)
            return;
        let title = notification.title;
        let tab = `${TAB_SETTING}:`;
        if (title === 'Food Expiration')
            tab = `${TAB_INVENTORY}:`;
        else if (title === 'Analysis')
            tab = `${TAB_STATISTIC}:`;

        this.setTab(tab.split(':')[0]);
    }


    componentDidMount() {
        super.componentDidMount();

        // check and store token
        LOG(this, 'Checking if Token is registered with this account');
        getPushTokenRegis().then(res => {
            if (!res) {
                LOG(this, 'Push Token is not registered');
                getPushToken().then(token => {
                    if(!token) {
                        LOG(this, 'No Token Stored, no registration');
                    } else {
                        LOG(this, 'Trying to Register this device Push Token');
                        let os = Platform.OS;
                        registerPushToken('me', new PushNotiToken(token, os), this, 'pushNotiRegis')
                    }
                });
            } else {
                console.log('Push Token is already registered with this account, no registration needed');
            }
        });

        // add it self to push notification topic
        LOG(this, `subsribe to ${PUSH_NOTI_TOPIC} topic and waiting for notification`);
        PubSubManager.addSubscriber(PUSH_NOTI_TOPIC, this.handleNotification);

        // check if there is notification while the app is not active, then send to subscribers, which is home page for now
        LOG(this, `check for the last push notification`);
        getPushNoti().then(notification => {
            if (notification) {
                LOG(this, `there is available notification`);
                console.log(notification);
                LOG(this, `sending notification to subscribers`);
                PubSubManager.sendDataToTopic(PUSH_NOTI_TOPIC, notification);
                LOG(this, 'Erasing last push notification');
                setPushNoti(null);
            }
        });
    }

    componentWillUnmount(){
        PubSubManager.removeSubscriber(PUSH_NOTI_TOPIC, 0);
    }

    onSuccess(res, tag) {
        if (tag === 'pushNotiRegis') {
            setPushTokenRegis(true);
        }
    }

    onHeaderRightClicked() {
        super.onHeaderRightClicked();
        console.log(this.props.navigation);
        this.props.navigation.navigate("AddItem");
    }

    setTab(tabId) {
        this.setState({selectedTab: tabId});
        LOG(this, 'on tab: ' + tabId);

    }

    render() {
        const selectedTab = this.state.selectedTab;
        let screenContent = null;
        if (selectedTab === TAB_INVENTORY)
            screenContent = <InventoryTabNavigator screenProps={{
                rootNavigation: this.props.navigation
            }}/>;
        else if (selectedTab === TAB_STATISTIC)
            screenContent = <StatisticNavigator screenProps={{
                rootNavigation: this.props.navigation
            }}/>;
        else if (selectedTab === TAB_SHOPPING)
            screenContent = <ShoppingNavigator screenProps={{
                rootNavigation: this.props.navigation
            }}/>;
        else if (selectedTab === TAB_SETTING)
            screenContent = <SettingNavigator screenProps={{
                rootNavigation: this.props.navigation
            }}/>;


        return (

            <Container style={styles.container}>
                <StatusBar
                    backgroundColor="#0098cc"
                    barStyle="light-content"
                />
                {screenContent}
                <Footer>
                    <FooterTab style={styles.footer}>
                        <Button title="inventory"  onPress={() => this.setTab(TAB_INVENTORY)}
                                style={[this.state.selectedTab === TAB_INVENTORY ? styles.selectedfooter : ""]}>
                            <Icon name="home" style={styles.icon}/>
                        </Button>
                        <Button title="statistics" onPress={() => this.setTab(TAB_STATISTIC)}
                                style={[this.state.selectedTab === TAB_STATISTIC ? styles.selectedfooter : ""]}>
                            <Icon name="stats" style={styles.icon}/>
                        </Button>
                        {/*<Button title="shopping" onPress={() => this.setTab(TAB_SHOPPING)}*/}
                                {/*style={[this.state.selectedTab === TAB_SHOPPING ? styles.selectedfooter: ""]}>*/}
                            {/*<Icon name="list-box" style={styles.icon}/>*/}
                        {/*</Button>*/}
                        <Button title="setting" onPress={() => this.setTab(TAB_SETTING)}
                                style={[this.state.selectedTab === TAB_SETTING ? styles.selectedfooter : ""]}>
                            <Icon name="settings" style={styles.icon}/>
                        </Button>
                        <Button title="inventory" onPress={() => this.props.navigation.navigate("AddItem")}>
                            <Icon name="add-circle" style={styles.icon}/>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#F5F5F5',
    },
    footer: {
        backgroundColor: '#5DC9FF',
    },
    selectedfooter: {
        backgroundColor: '#0098cc',
    },
    icon: {
        color: 'white'
    }
});
