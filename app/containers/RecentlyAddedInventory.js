import React from "react";
import {SectionList, View} from "react-native";
import {    Content,    Text,    ListItem,} from "native-base";
import {ItemsListCell} from "../components/ItemsListCell";
import {fromToday} from "../utils/Time";
import {FoodChronogization} from "../utils/Inventory";
import {getFoodInputDate} from "../services/FoodItemsServices";
import constants from "../utils/Constants";
import Moment from "moment/moment";
import StatefulBaseContainer from "./StatefulBaseContainer";
import styles from "../Styles"
import {groupBy} from "../utils/Heplers";

export class RecentlyAddedInventory extends StatefulBaseContainer {
    static navigationOptions = ({navigation, screenProps}) => ({
        title: "Recently Added",
        tabBarLabel: "Recently Added",
        header: null
    });

    constructor(props) {
        super(props);
        this._updateItem = this._updateItem.bind(this);
        this.state = {
            stateStorageKey: constants.RECENTLY_ADDED_INV_STATE,
            delayPerPull: 10,
            isLoading: true,
            inventory: [],
        }
    }

    refreshData() {
        super.refreshData();
        this.setState({inventory: []});
        this.setLoading(true);
        this.fetchData();
    }

    fetchData() {
        let time = fromToday(7);
        getFoodInputDate(time.start, 0, 'both', this);
    }

    onSuccess(res, tag) {
        let inventory = groupBy(res, item => item.input_date.slice(0, 16));
        inventory = Object.values(inventory)
            .map(arr => {return {'title': arr[0]['input_date'].slice(0, 16), data:arr}});
        this.setState({inventory});
        this.setLoading(false);
        this.updateLocalState(true);
    }

    _renderItem = ({item, index, section}) => {
        return (
            <ItemsListCell item={item} index={index} navigation={this.props.navigation}
            section={section} updateItem={this._updateItem}/>)
    };

    _updateItem({item, index, section}){
        section.data.splice(index, 1, item);
        this.updateLocalState();
    }

    render() {
        return (
            <View  style={[styles.pageContainer]}>
                <SectionList
                    refreshControl={this.showSpinner()}
                    renderItem={this._renderItem}
                    renderSectionHeader={({section: {title}}) =>
                        <View style={{backgroundColor:'#ECECEC', padding: 10, marginLeft:5}}>
                            <Text style={{fontSize: 16}}>{title}</Text>
                        </View>}
                    sections={this.state.inventory}
                    keyExtractor={(item) => item._id}
                    stickySectionHeadersEnabled={true}/>
            </View>

        );
    }
}
