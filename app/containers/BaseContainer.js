/**
 * Created by khoale on 8/23/17.
 */
import React from "react";
import {RefreshControl} from "react-native";
import {LOG} from "../utils/Heplers";

/**
 * Base container is a container which uses Base service to communicate with backend.
 */
export default class BaseContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
        };

        this.onHeaderRightClicked = this.onHeaderRightClicked.bind(this);
        this.onHeaderLeftClicked = this.onHeaderLeftClicked.bind(this);
        this.onSuccess = this.onSuccess.bind(this);
        this.onError = this.onError.bind(this);
        this.onRefresh = this.onRefresh.bind(this);
        this.refreshData = this.refreshData.bind(this);
    }

    /**
     * Log if right button in header is clicked
     */
    onHeaderRightClicked() {
        LOG(this, 'onHeaderRightClicked');
    }

    /**
     * log if left button in header is clicked
     */
    onHeaderLeftClicked() {
        LOG(this, 'onHeaderLeftClicked')
    }

    /**
     * set up left and right button header
     */
    componentDidMount() {
        LOG(this, 'componentDidMount');
        if(this.props.navigation)
            this.props.navigation.setParams({
                onHeaderRightClicked: this.onHeaderRightClicked,
                onHeaderLeftClicked: this.onHeaderLeftClicked,
            });
    }

    /**
     * on success will be called if the request is return without error
     * @param res respond stat
     * @param tag additional/optional tag is return
     */
    onSuccess(res, tag) {
        LOG(this, 'on suscess ----- ' + JSON.stringify(res) + " with tag: " + tag);
        this.setLoading(false);
    }

    /**
     * on failture will be called if the request is return with error code
     * @param error
     * @param tag
     */
    onError(error, tag) {
        if (error.code) {
            let errorCode = error.code;
            let errorMessage = error.message;

            LOG(this, "on err ----- with status code:" + errorCode + "\n and message:"
                + JSON.stringify(errorMessage) + " with tag: " + tag);
        }

        this.setLoading(false);
    }

    /**
     * to show the spinning/loading icon or not
     * @param isLoading
     */
    setLoading(isLoading) {
        this.setState({isLoading: isLoading})
    }


    /**
     * check loading state
     * @returns {boolean}
     */
    isLoading(){
        return this.state.isLoading;
    }

    /**
     * refreshControl configuration
     * @returns {*}
     */
    showSpinner() {
        return <RefreshControl
            refreshing={this.isLoading()}
            onRefresh={this.onRefresh}
            tintColor="#FF0000"
            title="Loading..."
            titleColor="#00FF00"
            colors={['#FF0000', '#00FF00', '#0000FF']}
            progressBackgroundColor="#FFFFFF"
        />
    }

    /**
     * implement this to refresh data
     */
    refreshData(){
        LOG(this, "refreshing Data");
    }

    /**
     * called by refreshControl
     */
    onRefresh(){
        LOG(this, "onRefresh");
        this.refreshData();
    }
}
