import React from "react";
import {View, Text} from "react-native";
import {Content} from 'native-base';
import {VictoryChart, VictoryBar, VictoryZoomContainer, VictoryLabel, VictoryAxis} from "victory-native";
import {fromToday} from "../utils/Time";
import {getFoodPrint} from "../services/AnalysisServices";
import StatefulBaseContainer from "./StatefulBaseContainer";
import constants from "../utils/Constants";
import styles from "../Styles";

export default class MostWasted extends StatefulBaseContainer {
    static navigationOptions = ({navigation, screenProps}) => ({
        tabBarLabel: "Most Wasted",
    });

    constructor(props) {
        super(props);
        this.state = {
            stateStorageKey: constants.STATISTIC_MOST_WASTED_STATE,
            delayPerPull: 60,
            isLoading: true,
            count: 5,
            user_data: [],
            graph_data: []
        };
    }

    refreshData() {
        let time = fromToday(63);
        getFoodPrint(time.start, 0, 'monthly', 'user', this);
    }

    componentDidMount() {

    }

    onSuccess(res, tag) {
        if (res.length > 0) {
            this.setLoading(false);
            this.setState({user_data: res[0]}, () => this.fetchUserData());
            this.updateLocalState(true);
        }
    }

    fetchUserData() {
        let {count, user_data} = this.state;

        if (!user_data || !user_data.data)
            return;
        let items = Object.values(user_data.data).sort((a, b) => (b.wasted || 0) - (a.wasted || 0));
        let total_item = items.filter((item) => item.name === "total");
        let grand_total = total_item.length > 0 ? total_item[0].total : 0;

        if (items.length === 0)
            return;

        if (items.length > 0)
            items = items.filter(item => item.name !== "total");

        items.forEach((item) => {
            if (!("wasted" in item)) {
                item.wasted = 0;
            }
        });

        if (items.length > count) {
            items = items.slice(0, count);
        }

        this.setState({
            graph_data: items.map((item) => {
                return {
                    x: item.name,
                    y: grand_total > 0 ? item.wasted * 100 / grand_total : 0
                }
            })
        });
    }

    renderBarGraph() {
        if (this.state.user_data.length !== 0) {
            return (
                <VictoryChart
                    domainPadding={10}
                    containerComponent={
                        <VictoryZoomContainer/>
                    }
                >
                    <VictoryLabel x={20} y={30}
                                  style={{
                                      fontFamily: "inherit",
                                      fontSize: 18,
                                      fontStyle: "italic",
                                  }}
                                  text={"Percentage"}
                    />
                    <VictoryAxis
                        tickValues={this.state.graph_data.map(element => element.x)}
                        tickFormat={(x) => {
                            return x
                        }
                        }
                        style={{
                            axis: {strokeWidth: 2},
                            ticks: {
                                strokeWidth: 2
                            },
                            tickLabels: {
                                fontFamily: "inherit",
                                fontSize: 16
                            }
                        }}
                        domainPadding={{x: 10}}
                        padding={{top: 50, bottom: 60}}
                    />
                    <VictoryAxis
                        dependentAxis={true}
                        orientation={"left"}
                        style={{
                            grid: {
                                stroke: "#ccc",
                                strokeWidth: 1
                            },
                            axis: {strokeWidth: 2},
                            ticks: {strokeWidth: 2},
                            tickLabels: {
                                fontFamily: "inherit",
                                fontSize: 16
                            }
                        }}
                    />
                    <VictoryBar
                        style={{data: {fill: "#E2252B"}}}
                        data={this.state.graph_data}
                        // domain={{y: [0, 100]}}
                    />
                </VictoryChart>
            );
        }
    }

    render() {
        if (!this.state.graph_data.length)
            return (
                <View pointerEvents="none" style={{
                    backgroundColor: '#fff',
                    borderRadius: 10,
                    margin: 5,
                    borderColor: '#D8D8D8',
                    borderWidth: 2,
                    paddingTop: 15,
                    paddingBottom: 15,
                }}>
                    <Text style={{color: '#0E121F', fontSize: 18, fontWeight: '400', textAlign: 'center'}}>
                        No data for this month
                    </Text>
                </View>
            );
        return (
            <Content refreshControl={this.showSpinner()} style={styles.pageContainer}>
                <View pointerEvents="none" style={{
                    backgroundColor: '#fff',
                    borderRadius: 10,
                    margin: 5,
                    borderColor: '#D8D8D8',
                    borderWidth: 2,
                    paddingTop: 15,
                }}>
                    <Text style={{color: '#0E121F', fontSize: 18, fontWeight: '400', textAlign: 'center'}}>
                        Top 5 most wasted food items this month
                    </Text>
                    {this.renderBarGraph()}
                </View>
            </Content>
        )
    }
}