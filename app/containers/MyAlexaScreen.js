/**
 * Created by Azhad on 04/07/18.
 */
import React from "react";
import {DatePickerIOS, StyleSheet, Text, View} from "react-native";
import {Body, Button, Container, Content, Icon, Input, List, ListItem, Right} from "native-base";
import BaseContainer from "./BaseContainer";
import {getAuthUser} from "../utils/AuthUtils";
import {LOG} from "../utils/Heplers";
import moment from "moment";
import {getUserProfile, getAlexaCode, removeAlexaId} from "../services/UserServices";
import {ItemTextButton} from "../components/ItemButton";

export default class MyAlexaScreen extends BaseContainer {

    static navigationOptions = ({navigation, screenProps}) => {
        const params = navigation.state.params || {};

        return {
            headerTitle: 'My Alexa',
            headerTintColor: '#343d5c',
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            alexa_id: '',
            alexa_code: {}
        };
    }

    componentDidMount() {
        super.componentDidMount();
        this.loadData();
    }

    onSuccess(res, tag) {
        if (tag === "code")
            this.setState({alexa_code: res.alexa_code});
        else if (tag === "delete")
            this.setState({alexa_id: ''});
        else
            this.setState({alexa_id: res.alexa_id || ''});

    }

    loadData() {
        getAuthUser().then(res => {
            if (res) {
                this.setState({user_id: res._id});
                getUserProfile([res._id], this);
            }
        });
    }

    _render_show_code() {
        return (
            <View style={{flex:1, justifyContent: 'center'}}>
                <Text style={{fontSize: 30, fontWeight: '500', textAlign: 'center', padding: 30, color: '#343d5c'}}>
                    Code : {this.state.alexa_code.code}
                </Text>
                <Text style={{fontSize: 18, fontWeight: '400', textAlign: 'center', padding: 30}}>
                    Valid till : {moment.utc(this.state.alexa_code.exp).local().format('LTS')}
                </Text>
                <View style={{flex:1, justifyContent: 'center', paddingLeft:80, paddingRight:80}}>
                    <ItemTextButton text={'GET THE CODE AGAIN'} textStyle={{padding:10}}
                                    onPress={() => getAlexaCode(this.state.user_id, this, 'code')}/>
                </View>
            </View>
        )
    }

    _render_get_code() {
        if(this.state.alexa_code && this.state.alexa_code.code)
            return this._render_show_code();
        return (
            <View style={{flex:1, justifyContent: 'center'}}>
                <Text style={{fontSize: 18, fontWeight: '400', textAlign: 'center', padding: 30}}>
                    Connect with an alexa device
                </Text>
                <View style={{flex:1, justifyContent: 'center', paddingLeft:80, paddingRight:80}}>
                    <ItemTextButton text={'GET CODE TO CONNECT'} textStyle={{padding:10}}
                                    onPress={() => getAlexaCode(this.state.user_id, this, 'code')}/>
                </View>
            </View>
        )
    }

    _render_delete() {
        return (
            <View style={{}}>
                <View style={{flex:1, justifyContent: 'center'}}>
                    <Text style={{fontSize: 18, fontWeight: '400', textAlign: 'center', padding: 30}}>
                        You are connected with an alexa device
                    </Text>
                    <View style={{flex:1, justifyContent: 'center', paddingLeft:80, paddingRight:80}}>
                        <ItemTextButton text={'REMOVE ALEXA DEVICE'} textStyle={{padding:10, fontSize: 14}}
                                        onPress={() => removeAlexaId(this.state.user_id, this, 'delete')}/>
                    </View>
                </View>
            </View>


        )
    }

    render() {
        return (
            <Content>
                {this.state.alexa_id !== '' ? this._render_delete() : this._render_get_code()}
            </Content>
        )
    }
}