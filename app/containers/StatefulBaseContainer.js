import BaseContainer from "./BaseContainer";
import {getComponentState, LOG, setComponentState} from "../utils/Heplers";
import Moment from "moment/moment";


export default class StatefulBaseContainer extends BaseContainer {

    constructor(props) {
        super(props);
        // these state need to be set up
        this.state = {
            // when is the last pull
            lastPull: null,
            // name of key in async storage
            stateStorageKey: null,
            // interval between pulls, default to 5 minutes
            delayPerPull: null,
        }
    }

    /**
     * To display a warning in console if there is no state storage key is specified.
     * @private
     */
    _checkStateStorageKeyWarning() {
        if (!this.state.stateStorageKey)
            console.warn(
                `stateStorageKey has not been set for ${this.constructor.name}, state will not be stored or pulled locally.` +
                ' BaseContainer can be used instead? ');
    }

    /**
     * Checking the time of last pull and pull new data if time expired
     * If time not expired then pull the data from local storage.
     */
    componentWillMount() {
        this._checkStateStorageKeyWarning();
        let stateKey = this.state.stateStorageKey;
        if (!stateKey) {
            this.refreshData();
        }
        else {
            getComponentState(stateKey)
                .then(state => {
                    let shouldRefresh = false;

                    if (!state) {
                        LOG(this, 'No state found, going to refresh data');
                        state = {};
                        shouldRefresh = true;
                    }

                    if (!state.lastPull) {
                        LOG(this, 'No time info about last pull, going to refresh data');
                        shouldRefresh = true;
                    }

                    if (!this.state.delayPerPull)
                        LOG(this, 'No delayPerPull is set, 5 minutes will be used as default');

                    let delayPerPull = this.state.delayPerPull || 5;
                    let diff = Moment().diff(Moment(state.lastPull), 'minutes');

                    if (diff > delayPerPull) {
                        LOG(this, `time since last pull has passed ${delayPerPull} minutes, going to refresh data`);
                        shouldRefresh = true;
                    }

                    if (shouldRefresh) {
                        // refresh state with new data
                        this.refreshData();
                    } else {
                        // loading the whole state state
                        LOG(this, 'local state found! Loading State');
                        // let {delayPerPull, stateStorageKey} = this.state;

                        // this.setState({...state, delayPerPull, stateStorageKey});
                        delete state.delayPerPull;
                        delete state.stateStorageKey;
                        this.setState({...state});
                        this.setLoading(false);
                    }
                });
        }
    }

    /**
     * Update the current state to the local storage before the component unmounts.
     */
    componentWillUnmount() {
        LOG(this, 'Component Will Unmount');
        this.updateLocalState();
    }

    /**
     * call whenever there is an update of state
     * doesn't matter if it is setState or not
     * THIS WILL BE CALLED IN COMPONENT WILL MOUNT
     */
    updateLocalState(didPull = false) {
        this._checkStateStorageKeyWarning();
        let stateKey = this.state.stateStorageKey;
        if (stateKey) {
            if (didPull)
                this.state.lastPull = Moment();
            setComponentState(stateKey, this.state);
        }
    }
}