import React from "react";
import {View, FlatList, ScrollView} from "react-native";
import {
    Content,
    Text,
    Root,
    ListItem
} from "native-base";
import BaseContainer from "./BaseContainer";
import {AddItemRow} from "../components/AddItemRow";
import {PubSubManager} from "../utils/PubSubManager";
import constants from "../utils/Constants";
import {Fruits, Vegetables, Dairies, Meats, Grains, Legumes, getDaysFromType, getType} from "../utils/FoodItem";
import AutoSuggest from "../components/AutoSuggest";
import {ItemTextButton} from "../components/ItemButton";
import {getSuggestiveShoppingList} from "../services/RecommendationServices";
import styles from "../Styles";
import {FoodIcon} from "../components/TouchableThumbnail";


export class AddItemScreen extends BaseContainer {

    itemId = 0;
    static navigationOptions = ({navigation, screenProps}) => {
        const params = navigation.state.params || {};
        return {
            title: "Food Item",
            headerTintColor: 'white',
            headerStyle: {backgroundColor: '#5DC9FF'},
            headerRight:
                <ItemTextButton transparent text={'SAVE'} onPress={params.onHeaderRightClicked}
                                buttonStyle={{paddingLeft: 15, paddingRight: 15}} textStyle={{fontSize: 14}}/>
        }
    };

    constructor(props) {
        super(props);
        this.deleteItemInState = this.deleteItemInState.bind(this);
        this._addNewRowItem = this._addNewRowItem.bind(this);
        this.state = {
            data: {
                "shopping_list": [],
                "your_list": [],
                "suggestive_shopping_list": []
            },
            showToast: false,
        }
    }

    componentDidMount() {
        super.componentDidMount();
        PubSubManager.createTopic(constants.ADD_ITEM);
        PubSubManager.createTopic(constants.ADD_ITEM_SAVE);
        getSuggestiveShoppingList(this, 'SHOPPING');
    }

    componentWillUnmount() {
        PubSubManager.removeTopic(constants.ADD_ITEM);
        PubSubManager.removeTopic(constants.ADD_ITEM_SAVE);
    }

    deleteItemInState(sectionName, index) {
        let data = this.state.data;
        data[sectionName].splice(index, 1);
        this.setState({data: data});
    }

    getNewItem() {
        return {
            id: this.getId().toString(),
            type: [],
            days: '0',
        };
    }

    getId() {
        return this.itemId++
    }

    onHeaderRightClicked() {
        super.onHeaderRightClicked();
        PubSubManager.sendDataToTopic(constants.ADD_ITEM, null);
        PubSubManager.sendDataToTopic(constants.ADD_ITEM_SAVE, null);

    }

    onSuccess(res, tag) {
        if (tag === 'SHOPPING') {
            this.setState({
                data: {...this.state.data, ...{suggestive_shopping_list: res}},
            });
        }
    }

    _renderShoppingItem = ({item, index}) => {
        return <AddItemRow item={item}
                           index={index}
                           deleteItemInState={() => this.deleteItemInState('shopping_list', index)}/>
    };

    _renderYourItem = ({item, index}) => {
        return <AddItemRow item={item}
                           index={index}
                           deleteItemInState={() => this.deleteItemInState('your_list', index)}/>
    };


    _addNewRowItem(name) {
        let data = this.state.data;
        let item = this.getNewItem();
        item.name = name;
        item.type = getType(name);
        item.quantity = '1';
        item.days = getDaysFromType(item.type);
        data.your_list.push(item);
        this.setState({data: data});
    }

    _createSuggestiveShoppingList() {
        let data = this.state.data;
        this.state.data.suggestive_shopping_list.forEach((name) => {
            let type = getType(name);
            if (!type) return;
            let item = this.getNewItem();
            item.name = name;
            item.type = type;
            item.quantity = '0';
            item.days = getDaysFromType(type);
            data.shopping_list.push(item);
        });
        return data;
    }

    _showSuggestiveShoppingList() {
        let data = this._createSuggestiveShoppingList();
        this.setState({data: data});
    }

    _renderSuggestiveItem({item, index}) {
        return (
            <View style={{height: 40, flexDirection: 'row', alignItems: 'center'}}>
                <FoodIcon name={item.toLowerCase()} size={32}/>
                <Text style={{fontSize: 14, color: 'black', paddingLeft: 10}}>
                    {item}
                </Text>
            </View>
        )
    }

    _renderShoppingList() {
        let suggestive_shopping_list = this.state.data.suggestive_shopping_list;
        let shopping_list = this.state.data.shopping_list;
        if (suggestive_shopping_list.length <= 0) return null;
        if (shopping_list.length <= 0)
            return (
                <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', padding: 5}}>
                    <ListItem bordered>
                        <Text style={{fontSize: 15}}>SUGGESTIVE SHOPPING LIST</Text>
                    </ListItem>
                    <FlatList
                        data={this.state.data.suggestive_shopping_list}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={this._renderSuggestiveItem}/>
                    <View style={{flex: 1, padding: 5, flexDirection: 'row', justifyContent: 'center'}}>
                        <ItemTextButton text={'USE SUGGESTIVE SHOPPING LIST'} textStyle={{fontSize: 14}}
                                        onPress={this._showSuggestiveShoppingList.bind(this)}
                                        buttonStyle={{paddingLeft: 15, paddingRight: 15}}/>
                    </View>
                </View>
            );
        return (
            <View>
                <ListItem bordered>
                    <Text style={{fontSize: 15}}>SUGGESTIVE SHOPPING LIST</Text>
                </ListItem>
                <FlatList
                    data={shopping_list}
                    keyExtractor={(item) => item.id}
                    renderItem={this._renderShoppingItem}
                />
                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', padding: 5, zIndex: 1}}>
                    <ItemTextButton danger text={'REMOVE SUGGESTIVE SHOPPING LIST'} textStyle={{fontSize: 14}}
                                    buttonStyle={{paddingLeft: 15, paddingRight: 15}}
                                    onPress={() => {
                                        this.setState({
                                            data: {...this.state.data, ...{shopping_list: []}}
                                        });
                                    }}/>
                </View>
            </View>
        );
    }

    _renderYourList() {
        let yourList = this.state.data.your_list;
        if (yourList.length <= 0) {
            return (
                <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', padding: 5}}>
                    <ListItem bordered>
                        <Text style={{fontSize: 15}}>YOUR LIST</Text>
                    </ListItem>
                    <Text style={{color: '#95989A', fontSize: 14}}>

                        Add your first item by start typing in the search bar and select our supported food
                    </Text>
                </View>
            );
        } else {
            return (
                <View>
                    <ListItem bordered>
                        <Text style={{fontSize: 15}}>YOUR LIST</Text>
                    </ListItem>
                    <FlatList
                        style={{}}
                        data={yourList}
                        keyExtractor={(item) => item.id}
                        renderItem={this._renderYourItem}
                    />
                </View>
            );
        }
    }

    render() {
        return (
            <Root>
                <AutoSuggest suggestions={[...Fruits, ...Vegetables, ...Dairies, ...Meats, ...Grains, ...Legumes]}
                             place_holder="Search to add food by name"
                             resetInputOnSelect
                             onSelectSuggestion={this._addNewRowItem}/>
                <Content style={[styles.pageContainer, {marginTop: 50}]}
                         innerRef={ref => this.AddContent = ref}
                         onContentSizeChange={() => this.AddContent.scrollToEnd({animated: true})}>
                    {this._renderShoppingList()}
                    {this._renderYourList()}
                </Content>
            </Root>
        )
    }
}