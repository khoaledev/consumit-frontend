/**
 * Created by khoale on 8/11/17.
 */
import React from "react";
import {DatePickerAndroid, Text, View, Alert} from "react-native";
import {Body, Button, Container, Content, Icon, Input, List, ListItem, Right} from "native-base";
import BaseContainer from "./BaseContainer";
import {UserProfileUpdate} from "../models/requests/UserProfileUpdate";
import {LOG} from "../utils/Heplers";
import {getUserProfile, updateUserProfile} from "../services/UserServices";

export default class MyAccountEditScreen extends BaseContainer {
    static navigationOptions = ({navigation, screenProps}) => {
        const params = navigation.state.params || {};
        return {
            headerTitle: 'My Account',
            headerTintColor: '#343d5c',
            headerRight: (
                <Button transparent
                        style={{
                            marginRight: 10,
                            padding: 5,
                            paddingLeft: 10,
                            paddingRight:10,
                            borderColor: '#5DC9FF',
                            borderWidth: 2,
                            marginTop: 5,
                            borderRadius: 5
                        }}
                        onPress={() => params.onHeaderRightClicked()}>
                    <Text>Save</Text>
                </Button>)
        }
    };

    constructor(props) {
        super(props);
        // let userProfile = this.props.navigation.state.params.profile;
        this.state = {
            // date: new Date(Date.parse(userProfile.date_of_birth)),
            // showPicker: false,
            // email: userProfile.email,
            // age: userProfile.age,
            // gender: userProfile.gender,
            display_name: '',
            // date_of_birth: userProfile.date_of_birth,
        };
    }

    onHeaderRightClicked() {
        super.onHeaderRightClicked();
        let userProfileUpdate = new UserProfileUpdate();
        userProfileUpdate.display_name = this.state.display_name;
        updateUserProfile('me', userProfileUpdate, this, 'updateProfile')
    }

    componentDidMount() {
        super.componentDidMount();
        getUserProfile('me', this, 'userProfile');
    }

    onSuccess(res, tag) {
        // if (tag === 'userProfile')
        //     this.setState({display_name: res.display_name});
        if (tag === 'updateProfile') {
            LOG(this, "Successfully update profile " + JSON.stringify(res));
            Alert.alert(
                '',
                'Successfully update profile',
                [
                    {text: 'GoBack', onPress: () => this.props.navigation.goBack()},
                    {text: 'Ok', onPress: () => console.log("Ok")}
                ]);
            // this.setState({})
            // this.props.navigation.goBack();
        }
        this.setState({display_name: res.display_name});
    }

    // initDatePicker() {
    //     if (this.state.showPicker) {
    //         let dob = this.state.date_of_birth;
    //         if (!dob)
    //             dob = '1988-12-10';
    //
    //         DatePickerAndroid.open({date: new Date(dob.split('-'))})
    //             .then(({action, year, month, day}) => {
    //                 if (action !== DatePickerAndroid.dismissedAction) {
    //                     dob = [year, month, day];
    //                     this.setState({date: new Date(dob), showPicker: !this.state.showPicker});
    //                 }
    //             });
    //     }
    // }

    render() {
        return (
            <Container>
                <Content>
                    <List>
                        <ListItem icon>
                            <Body>
                            <Text>Display Name</Text>
                            </Body>
                            <Input style={{textAlign: 'right'}} value={this.state.display_name}
                                   onChangeText={text => this.setState({display_name: text})}/>
                        </ListItem>

                        {/*<ListItem icon>*/}
                        {/*<Body>*/}
                        {/*<Text>Age</Text>*/}
                        {/*</Body>*/}
                        {/*<Input style={{textAlign: 'right'}} value={this.state.age} onChangeText={text =>*/}
                        {/*this.setState({age: parseInt(text)})}/>*/}
                        {/*</ListItem>*/}

                        {/*<ListItem icon>*/}
                        {/*<Body>*/}
                        {/*<Text>Gender</Text>*/}
                        {/*</Body>*/}
                        {/*<Input style={{textAlign: 'right'}} value={this.state.gender} onChangeText={text =>*/}
                        {/*this.setState({gender: text})}/>*/}
                        {/*</ListItem>*/}

                        {/*<ListItem icon onPress={() => this.setState({showPicker: !this.state.showPicker})}>*/}
                        {/*<Body>*/}
                        {/*<Text>Day of birth</Text>*/}
                        {/*</Body>*/}
                        {/*<Right>*/}
                        {/*<Text>{this.state.date.toDateString()}</Text>*/}
                        {/*<Icon name="arrow-forward"/>*/}
                        {/*</Right>*/}
                        {/*</ListItem>*/}

                        {/*{this.initDatePicker()}*/}
                    </List>
                </Content>
            </Container>
        )
    }
}