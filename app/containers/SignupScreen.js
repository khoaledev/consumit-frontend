/**
 * Created by khoale on 8/6/2017.
 */
import React from "react";
import {Alert, Image} from "react-native";
import {Button, Container, Content, Form, Input, Item, Label, Text} from "native-base";
import Login from "../models/requests/Login";
import {sendRequest} from "../services/BaseServices";
import BaseContainer from "./BaseContainer";
import {UserProfile} from "../models/UserProfile";
import {setAuthUSer} from "../utils/AuthUtils";
import {TextButton} from "../components/TextButton";
import {SecureInput} from "../components/SecureInput";
import {navigationReset} from "../utils/NavigationUtils";
import {LOG} from "../utils/Heplers";
import {signup} from "../services/UserServices";


/**
 *
 */
export default class SignupScreen extends BaseContainer {
    // Nav options can be defined as a function of the screen's props:
    static navigationOptions = ({navigation}) => ({
        headerTitle: 'Sign Up',
        headerTintColor: 'white',
        headerStyle: {backgroundColor: '#5DC9FF'}
    });

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            // username: 'khoa@consumit.com',
            // password: 'P@ssw0rd',
            // confirm: 'P@ssw0rd'
            username: '',
            password: '',
            confirm: ''
        };

        this.signup = this.signup.bind(this);
    }

    onSuccess(res, tag) {
        super.onSuccess(res);

        LOG(this, "Saving user authentication to local");
        setAuthUSer(res);

        // TODO: reset navigation
        // this.props.navigation.navigate("Home");
        navigationReset("Home", this.props.navigation);
        //TODO: Handle 400
    }

    onError(res, tag) {
        LOG(this, 'Enter onError');

        if (res.status === 409) {
            Alert.alert(
                '',
                `${this.state.username} is already taken`,
                [
                    {text: 'Retry', onPress: () => console.log("Retry")}
                ]);
        } else if (res.status === 400) {
            Alert.alert(
                '',
                'invalid password or email')
        }
    }

    signup() {
        if (this.state.password === this.state.confirm) {
            this.setLoading(true);
            signup(new Login(this.state.username, this.state.password), this, 'signup');
        } else {
            Alert.alert(
                '',
                `confirm password does not match`,
                [
                    {text: 'Retry', onPress: () => console.log("Retry")}
                ]
            )
        }
    }

    /**
     *
     * @returns {XML}
     */
    render() {
        return (
            <Container>
                <Content style={{backgroundColor: 'white', paddingTop: 50}}>
                    <Image source={require('../assets/Images/logo.png')} style={{alignSelf: "center"}}/>

                    <Item stackedLabel style={{height:70}}>
                        <Label>Email</Label>
                        <Input numberOfLines={1} value={this.state.username}
                               onChangeText={(text) => this.setState({username: text})}/>
                    </Item>

                    <Item stackedLabel style={{height:70}}>
                        <Label>Password</Label>
                        <SecureInput value={this.state.password}
                                     onChangeText={(text) => this.setState({password: text})}/>
                    </Item>

                    <Item stackedLabel last style={{height:70}}>
                        <Label>Password Confirmation</Label>
                        <SecureInput value={this.state.confirm}
                                     onChangeText={(text) => this.setState({confirm: text})}/>
                    </Item>

                    <TextButton onPress={() => this.signup()} style={{alignSelf: "center"}}
                                size={"medium"} text={"Sign Up"}/>
                </Content>
            </Container>
        );
    }
}

