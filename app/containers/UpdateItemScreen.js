import React, {Component} from "react";
import BaseContainer from "./BaseContainer";
import {Container, Content, Button, Text, List, ListItem, Body, Input, Icon, Right, Label, Item} from "native-base";
import ActionPicker from "react-native-picker"
import {UpdateFoodItem} from "../models/requests/FoodItem";
import {updateFoodItem} from "../services/FoodItemsServices";


export class UpdateItemScreen extends BaseContainer {
    static navigationOptions = ({navigation, screenProps}) => {
        const params = navigation.state.params || {};
        return {
            title: "Update Item",
            headerRight:
                <Button transparent primary onPress={() => params.onHeaderRightClicked()}>
                    <Text>Save</Text>
                </Button>
        }
    };

    constructor(props) {
        super(props);
        let item = this.props.navigation.state.params.item;
        this.state = {
            _id: item._id,
            name: item.name,
            quantity: item.quantity,
            left: item.quantity,
            amount: '',
            action: ['eat'],
            message: '',
        }
    };

    onHeaderRightClicked() {
        super.onHeaderRightClicked();

        let updateItem = new UpdateFoodItem();
        updateItem.action = this.state.action[0];
        updateItem.message = this.state.message;
        if (this.state.amount <= 0)
            return;
        updateItem.amount = parseFloat(this.state.amount);
        updateFoodItem(this.state._id, updateItem, this);
    }

    onSuccess(){
        this.props.navigation.goBack();
    }

    /**
     * check if number or a string (of number) is les than limit
     * @param num : number or string represent a number
     * @param limit
     * @returns {boolean}
     * @private
     */
    _isNumInLimit(num, limit) {
        if (typeof  num === 'number')
            return (num <= limit && num >= 0);
        else if (typeof num === 'string'){
            let n = parseFloat(num) || 0;
            return (n <= limit && n >= 0);

        }
        else
            return false;
    }

    _onAmountChange = (amount) => {

        if (!isNaN(amount) && this._isNumInLimit(amount, parseFloat(this.state.quantity))) {
            this.setState({amount: amount});
            this.setState({left: this.state.quantity - parseFloat(amount)})
        }
    };

    _initActionPicker() {
        let pickerData = ['eat', 'throw', 'give', 'freeze'];
        ActionPicker.init({
            pickerTitleText: 'Action',
            pickerData: pickerData,
            selectedValue: this.state.action,
            onPickerConfirm: data => this.setState({action: data}),
            onPickerCancel: data => {

            },
            onPickerSelect: data => {

            },
        });
        ActionPicker.show()
    }

    render() {
        return (
            <Container>
                <Content>
                    <List>
                        <ListItem icon>
                            <Body>
                            <Text>Amount</Text>
                            </Body>
                            <Body>
                            <Input placeholder='0'
                                   numberOfLines={1}
                                   onChangeText={this._onAmountChange}
                                   keyboardType='numeric'
                                   style={{textAlign: 'right'}}/>
                            </Body>
                        </ListItem>
                        <ListItem icon
                                  onPress={() => this._initActionPicker()}>
                            <Body>
                            <Text>Action</Text>
                            </Body>
                            <Right>
                                <Text>{this.state.action}</Text>
                                <Icon name="ios-arrow-forward-outline"/>
                            </Right>
                        </ListItem>
                        <ListItem icon>
                            <Body>
                            <Text>quantity left</Text>
                            </Body>
                            <Body>
                            <Text style={{textAlign: 'right'}}>{this.state.left}</Text>
                            </Body>
                        </ListItem>
                    </List>
                    <Item stackedLabel>
                        {/*<Body>*/}
                        <Label>Message</Label>
                        {/*</Body>*/}
                        {/*<Body>*/}
                        <Input numberOfLine={3}
                               placeholder='delicious'
                               onChangeText={text => this.setState({message: text})}
                               style={{textAlign: 'right'}}
                               value={this.state.message}/>
                        {/*</Body>*/}
                    </Item>
                </Content>
            </Container>
        )
    }
}