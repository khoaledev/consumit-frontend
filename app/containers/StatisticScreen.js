import React from "react";
import {TabNavigator} from "react-navigation"
import {View, Text} from "react-native"
import {InventoryNavigator} from "./InventoryScreen";
import {
    VictoryChart,
    VictoryPie,
    VictoryZoomContainer,
    VictoryLine,
    VictoryLabel,
    VictoryAxis,
    VictoryLegend
} from "victory-native";
import {Content} from 'native-base';
import MostWasted from "./MostWasted";
import MostConsumed from "./MostConsumed";
import {fromToday} from "../utils/Time";
import Moment from "moment";
import {getFoodPrint} from "../services/AnalysisServices";
import StatefulBaseContainer from "./StatefulBaseContainer";
import constants from "../utils/Constants";
import styles from "../Styles";


export class FoodPrint extends StatefulBaseContainer {
    static navigationOptions = ({navigation, screenProps}) => ({
        tabBarLabel: "Food Print",
    });

    constructor(props) {
        super(props);
        this.state = {
            stateStorageKey: constants.STATISTIC_STATE,
            delayPerPull: 60,
            isLoading: true,
            loadUserAll: false,
            loadUserData: false,
            loadGlobalData: false,
            user_data: [],
            global_data: [],
            user_all: {}
        };
    }

    refreshData() {
        this.setLoading(true);
        this.setState({
            loadUserAll: false,
            loadUserData: false,
            loadGlobalData: false,
            user_data: [],
            global_data: [],
            user_all: {}
        });
        this.fetchData();
    }

    fetchData() {
        let time = fromToday(100);
        getFoodPrint(time.start, 0, 'monthly', 'user_all', this, 'user_all');
        getFoodPrint(time.start, 0, 'monthly', 'user', this, 'user');
        getFoodPrint(time.start, 0, 'monthly', 'global', this, 'global');

    }

    onSuccess(res, tag) {
        if (tag === "global")
            this.setState({global_data: res, loadGlobalData: true}, this.checkLoadAll);
        else if (tag === "user")
            this.setState({user_data: res, loadUserData: true}, this.checkLoadAll);
        else if (tag === "user_all")
            this.setState({user_all: res, loadUserAll: true}, this.checkLoadAll);
        this.updateLocalState(true);
    }

    checkLoadAll() {
        if (this.state.loadUserAll && this.state.loadUserData && this.state.loadGlobalData)
            this.setLoading(false);
    }

    fetchUserData() {
        return this.state.user_data.map((item) => {
            return {
                x: Moment.utc(item.start_time),
                y: item.data.total.total > 0 ? item.data.total.wasted * 100 / item.data.total.total : 0
            }
        });
    }

    fetchGlobalData() {
        return this.state.global_data.map((item) => {
            return {
                x: Moment.utc(item.start_time),
                y: item.data.total.total > 0 ? item.data.total.wasted * 100 / item.data.total.total : 0
            }
        });
    }

    renderPieChart() {
        if (!(Object.keys(this.state.user_all).length === 0)) {
            let {user_all} = this.state;
            return (
                <View style={{
                    flex: 1,
                    paddingBottom: 15,
                    backgroundColor: '#fff',
                    borderRadius: 10,
                    borderColor: '#D8D8D8',
                    borderWidth: 2,
                    paddingTop: 15,
                    margin: 5,
                    marginBottom: 0,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <Text style={{fontSize: 18, fontWeight: '400', textAlign: 'center', color: '#0E121F'}}>
                        Consumed vs Wasted
                    </Text>

                    <VictoryPie
                        colorScale={["#EB5050", "#29C778"]}
                        data={[
                            {x: "Wasted", y: Math.round(user_all.analysis.perc_wasted)},
                            {x: "Consumed", y: Math.round(100 - user_all.analysis.perc_wasted)},
                        ]}
                        // padding={{left: 20, right: 20, top: 15, bottom: 10}}
                        labels={(d) => `${d.y} %`}
                    />
                    <VictoryLegend standalone={true}
                        // x={90} y={0}
                                   gutter={50}
                                   centerTitle
                        // style={{border: {stroke: "#343d5c"}}}
                                   orientation={"horizontal"}
                                   data={[
                                       {name: "Consumed", symbol: {fill: "#29C778", type: "square"}},
                                       {name: "Wasted", symbol: {fill: "#EB5050", type: "square"}}
                                   ]}
                                   height={25}
                                   padding={{top: 0, bottom: 0}}
                    />

                </View>
            );
        }
    }

    renderLineGraph() {
        if (this.state.user_data.length > 0 && this.state.global_data.length > 0) {
            return (
                <View style={{
                    paddingBottom: 15,
                    backgroundColor: '#fff',
                    borderRadius: 10,
                    borderColor: '#D8D8D8',
                    borderWidth: 2,
                    paddingTop: 15,
                    margin: 5,
                }}>
                    <Text style={{fontSize: 18, fontWeight: '400', textAlign: 'center', color: '#0E121F'}}>
                        Wasted % : You vs All Users
                    </Text>
                    <VictoryLegend standalone={true}
                        // x={300} y={10}
                                   gutter={10}
                                   centerTitle
                                   // style={{parent: {justifyContent: "center"}}}
                                   orientation={"vertical"}
                                   data={[
                                       {name: "You", symbol: {fill: "#5DC9FF", type: "square"}},
                                       {name: "All Users", symbol: {fill: "#5D2B0A", type: "square"}}
                                   ]}
                                   height={80}
                                   padding={0}
                    />
                    <VictoryChart domainPadding={2} scale={{y: "linear"}}
                                  style={{
                                      background: "#ccdee8",
                                      boxSizing: "border-box",
                                      display: "inline",
                                      padding: 0,
                                      fontFamily: "'Fira Sans', sans-serif",
                                      maxWidth: "50%",
                                      height: "auto"
                                  }}
                                  containerComponent={
                                      <VictoryZoomContainer/>
                                  }>
                        <VictoryLabel x={20} y={30}
                                      style={{
                                          fontFamily: "inherit",
                                          fontSize: 18,
                                          fontStyle: "italic",
                                      }}
                                      text={"Percentage"}
                        />
                        <VictoryAxis
                            tickValues={this.fetchUserData().map(element => element.x)}
                            tickFormat={(x) => {
                                return x.format("MMM YYYY")
                            }
                            }
                            style={{
                                axis: {strokeWidth: 2},
                                ticks: {
                                    strokeWidth: 2
                                },
                                tickLabels: {
                                    fontFamily: "inherit",
                                    fontSize: 16
                                }
                            }}
                        />
                        <VictoryAxis
                            dependentAxis={true}
                            orientation={"left"}
                            style={{
                                grid: {
                                    stroke: "#ccc",
                                    strokeWidth: 1
                                },
                                axis: {strokeWidth: 2},
                                ticks: {strokeWidth: 2},
                                tickLabels: {
                                    fontFamily: "inherit",
                                    fontSize: 16
                                }
                            }}
                        />
                        <VictoryLine
                            style={{
                                data: {stroke: "#5DC9FF", strokeWidth: 5},
                                // parent: {border: "1px solid #ccc"}
                            }}
                            domain={{
                                // x: [new Date(2018, 2, 1), new Date(2018, 4, 1)],
                                // y: [0, 20]
                            }}
                            scale={{x: "time", y: "linear"}}
                            data={this.fetchUserData()}
                            standalone={false}
                        />
                        <VictoryLine
                            style={{
                                data: {stroke: "#5D2B0A", strokeWidth: 5},
                            }}
                            domain={{
                                // x: [new Date(2018, 2, 1), new Date(2018, 4, 1)],
                                // y: [0, 20]
                            }}
                            scale={{x: "time", y: "linear"}}
                            data={this.fetchGlobalData()}
                            standalone={false}
                        />
                    </VictoryChart>
                </View>
            );
        }
    }

    render() {
        return (
            <Content refreshControl={this.showSpinner()} style={styles.pageContainer}>
                <View pointerEvents="none" style={{
                    flex: 1, flexDirection: 'column', justifyContent: 'center',
                    alignItems: "center", marginLeft: 5, marginRight: 5
                }}>
                    {this.renderPieChart()}
                    {this.renderLineGraph()}
                </View>
            </Content>
        );
    }
}

export const StatisticNavigator = TabNavigator({
    FoodPrint: {screen: FoodPrint},
    MostConsumed: {screen: MostConsumed},
    MostWasted: {screen: MostWasted}
}, {
    tabBarOptions: {
        activeTintColor: '#0098cc',
        inactiveTintColor: '#5DC9FF',
        labelStyle: {
            fontSize: 12,
        },
        style: {
            backgroundColor: 'white',
        },
        tabStyle: {
            height: 45,
        },
        indicatorStyle: {
            backgroundColor: '#0098cc'
        }
    }
});