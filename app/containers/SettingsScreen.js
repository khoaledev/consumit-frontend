/**
 * Created by khoale on 8/6/2017.
 */
import React from "react";
import {Platform} from "react-native";
import VectorIcon from "react-native-vector-icons/MaterialCommunityIcons";
import {StackNavigator} from "react-navigation";
import MyAccountScreen from "./MyAccountScreen";
import {Body, Container, Content, Icon, Left, List, ListItem, Right, Text} from "native-base";
import MyAccountEditScreen from "./MyAccountEditScreen";
import BaseContainer from "./BaseContainer";
import {AsyncStorage} from "react-native";
import MyAlexaScreen from "./MyAlexaScreen";
import {getPushToken, setPushToken} from "../utils/Users";
import {removePushToken} from "../services/UserServices";
import {PushNotiToken} from "../models/requests/User";
import {navigationReset} from "../utils/NavigationUtils";
import {logout} from "../utils/AuthUtils";
import {LOG} from "../utils/Heplers";

export class SettingsScreen extends BaseContainer {
    constructor(props) {
        super(props);
    }

    static navigationOptions = ({navigation, screenProps}) => ({
        // title: "Settings",
        header:null,
    });

    logout() {
        LOG(this, 'logging out');
        logout(this.props.navigation, this, "logout");
    }

    onSuccess(res, tag) {
        if (tag === 'logout') {
            getPushToken().then(token => {
                AsyncStorage.clear().then(() => {
                    setPushToken(token);
                    navigationReset("Landing", this.props.screenProps.rootNavigation);
                    LOG(this, 'LOG OUT');
                });
            });

        }
    }

    render() {
        return (
            <Content>
                <List>
                    <ListItem icon onPress={() => this.props.navigation.navigate("EditMyAccount")}>
                        <Left><VectorIcon name="account" size={30}/></Left>
                        <Body><Text>My Account</Text></Body>
                        <Right><Icon name="arrow-forward"/></Right>
                    </ListItem>

                    <ListItem icon onPress={() => this.props.navigation.navigate("MyAlexa")}>
                        <Left><Icon name="globe" size={30} style={{color:"#6E6E70"}}/></Left>
                        <Body><Text>My Alexa</Text></Body>
                        <Right><Icon name="arrow-forward"/></Right>
                    </ListItem>

                    <ListItem icon onPress={() => this.logout()}>
                        <Left><VectorIcon name="logout" size={30}/></Left>
                        <Body><Text>Sign Out</Text></Body>
                        <Right><Icon name="arrow-forward"/></Right>
                    </ListItem>
                </List>
            </Content>
        );
    }
}

export const SettingNavigator = StackNavigator({
        Settings: {screen: SettingsScreen},
        MyAccount: {screen: MyAccountScreen},
        MyAlexa: {screen: MyAlexaScreen},
        EditMyAccount: {screen: MyAccountEditScreen},
    },
    {headerMode: 'screen'}
);

{/*<SettingNavigator screenProps={{rootNavigation: InventoryNavigator}}/>*/
}