/**
 * Created by khoale on 8/6/2017.
 */
import {StyleSheet} from "react-native";

export default styles = StyleSheet.create({
    welcomeLogoContainer: {
        flex: 1,
        alignItems: 'center',
    },

    signUpInBtnsContainer: {
        flex: 1,
        flexDirection: 'row',
    },

    txtBtnTxt: {
        color: '#F39413',
        fontSize: 14,
    },

    txtBtn_common: {
        backgroundColor: '#F5F5F5',
        justifyContent: 'center',
        alignItems: 'center',
    },

    txtBtn_small: {
        borderRadius: 15,
        width: 100,
        height: 30,
        margin: 5,
    },


    txtBtn_medium: {
        borderRadius: 20,
        width: 135,
        height: 40,
        margin: 7,
    },

    txtBtnTxt_common: {
        color: '#F39413',
    },

    txtBtnTxt_small: {
        fontSize: 15,
    },

    txtBtnTxt_medium: {
        fontSize: 20,
    },

    signUpInBtnsTxt: {
        color: '#F39413',
        // fontWeight: 'bold',
        fontSize: 14,
        //fontFamily: 'Arial',
        textAlign: 'center',
    },


    //Sign in page styles
    signInLogoContainer: {
        flex: .4,
        flexDirection: 'column',
        //justifyContent: 'center',
        //alignItems: 'center',
        //top: 30,

    },

    signInLogo: {
        width: 150,
        height: 150,

    },

    signInFieldsContainer: {
        flex: 0.3,
        flexDirection: 'column',
        width: '80%'

    },

    signInField: {
        height: 40,
        borderColor: 'black',
        borderWidth: 2,
        paddingLeft: 3,
        marginBottom: 10

    },

    forgotPasswordLinkContainer: {
        flex: 0.2,
        flexDirection: 'column',

    },

    signInBtnContainer: {
        flex: 0.4,
        flexDirection: 'column',

    },

    signInBtn: {
        width: 150,
        height: 45

    },

    //Sign up page styles

    signUpLogoContainer: {
        flex: .2,
        flexDirection: 'column',
        //justifyContent: 'center',
        //alignItems: 'center',
        //top: 30,

    },

    signUpLogo: {
        width: 80,
        height: 80,

    },

    signUpFieldsContainer: {
        flex: 0.5,
        flexDirection: 'column',
        width: '80%'

    },

    signUpEmailTxt: {
        fontSize: 20,
        textAlign: 'center',
        paddingBottom: 10,
    },

    signUpField: {
        height: 40,
        borderColor: 'black',
        borderWidth: 2,
        paddingLeft: 3,
        marginBottom: 10

    },

    signUpBtnContainer: {
        flex: 0.4,
        flexDirection: 'column',

    },

    signUpBtn: {
        width: 150,
        height: 45

    },

    naviIcon: {
        width: 26,
        height: 26,
    },


    pageContainer:{
        flex: 1,
        backgroundColor: '#ECECEC'
    }
});

