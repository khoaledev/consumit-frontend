import React from "react";
import {sendRequestWithHeader, sendRequestWithHeaderAuth} from "./BaseServices";
import BaseContainer from "../containers/BaseContainer";
import {UpdateFoodItem} from "../models/requests/FoodItem";
import {LOG} from "../utils/Heplers";
import FoodItem from "../models/requests/FoodItem";

const USER_BASE_URL = 'food-items/';

/**
 * function for building the service url and attaching the parameters
 * @param url: to be added to the base url
 * @returns {string}
 */
function buildUserUrl(url: String = '') {
    if(url[0] === '?')
        return USER_BASE_URL.slice(0, -1) + url;
    if(url === "")
        return 'food-items';
    else
        return USER_BASE_URL + url;
}

/**
 * Get food items based on the parameters passed
 * @param start: start date
 * @param end: end date
 * @param expiry: boolean value to get expired or non expired item
 * @param context: BaseContainer model
 * @param tag: this extra tag will be passed back to onSuccess method of BaseContainer context
 */
export function getFoodInputDate(start: Number, end: Number, expiry: String, context: BaseContainer, tag) {
    let url = `?start=${start}&end=${end || 0}&expiry=${expiry}`;
    url = buildUserUrl(url);
    sendRequestWithHeaderAuth(null, 'GET', url, null, null, context, tag);
}

/**
 *
 * @param start: start date
 * @param end: end date
 * @param page: page number
 * @param size: size of the page
 * @param expiry
 * @param name
 * @param cat
 * @param context: BaseContainer model
 * @param tag: this extra tag will be passed back to onSuccess method of BaseContainer context
 */
export function getFoodExpiry(start: Number, end: Number, page: Number, size: Number, expiry: String, name = '', cat = '',
                              context: BaseContainer, tag) {
    let url =
        `expiry?start=${start}&end=${end || 0}&page=${page || 0}&size=${size || 10}&expiry=${expiry}` +
        `&name=${name}&cat=${cat}`;
    url = buildUserUrl(url);
    LOG(context, `getting food expiry with PAGE: ${page} and SIZE: ${size}`);
    sendRequestWithHeaderAuth(null, 'GET', url, null, null, context, tag);
}
export function createFoodItem(foodItem: FoodItem, context: BaseContainer, tag){
    let url = "";
    url = buildUserUrl(url);
    LOG(context, `adding item to db`);
    sendRequestWithHeaderAuth(null, "POST", url, foodItem, null, context, tag);
}

export function updateFoodItem(foodId: string, updateItem: UpdateFoodItem, context: BaseContainer, tag) {
    // TODO: test update food item
    let url = `${foodId}/quantity`;
    url = buildUserUrl(url);
    sendRequestWithHeaderAuth(null, 'PUT', url, updateItem, null, context, tag);
}