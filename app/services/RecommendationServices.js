import React from "react";
import {sendRequestWithHeaderAuth} from "./BaseServices";
import BaseContainer from "../containers/BaseContainer";
import {LOG} from "../utils/Heplers";

const USER_BASE_URL = 'recommendation/';

function buildUserUrl(url: String = '') {
    return USER_BASE_URL + url;
}

export function getSuggestiveShoppingList(context: BaseContainer, tag) {
    let url = `shopping-list`;
    url = buildUserUrl(url);
    LOG(context, `getting suggestive shopping list`);
    sendRequestWithHeaderAuth(null, 'GET', url, null, null, context, tag);
}