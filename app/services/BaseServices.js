import Request from "../models/requests/Request";
import {LOG} from "../utils/Heplers";
import Response from "../models/Response";
import BaseContainer from "../containers/BaseContainer";
import {getAuthUser} from "../utils/AuthUtils";

// const BASE_URL = 'http://54.152.119.134:5000/api/v1.0/';
const BASE_URL = 'http://consumit-TCP-load-balancer-e91db1be6adc1ff0.elb.us-east-1.amazonaws.com:5000/api/v1.0/';
// const BASE_URL = 'http://8cd39063.ngrok.io/api/v1.0/';

export const DEFAULT_TAG = 'default';

export function getBaseURL() {
    return BASE_URL
}

/**
 * Used for building the URL using the base url
 * @param path
 * @returns {string}
 */
export function buildRequestURL(path: String) {
    if (path[0] === '/')
        path = path.slice(1);
    return getBaseURL() + path;
}

/**
 * @param method: method of request
 * @param path: path of request, path will be appended to base_url
 * @param request: request body
 * @param response: response model
 * @param context: BaseContainer model
 * @param tag: this extra tag will be passed back to onSuccess method of BaseContainer context
 */
export function sendRequest(method: String, path: String, request: Request, response: Response
    , context: BaseContainer, tag = DEFAULT_TAG) {
    sendRequestWithHeader(null, method, path, request, response, context, tag);
}


/**
 * Send request without authentication
 * @param header: request header
 * @param method: request method
 * @param path: request path will be appended to base_url
 * @param request: request body
 * @param response: response model
 * @param context: BaseContainer model
 * @param tag: this extra tag will be passed back to onSuccess method of BaseContainer context
 */
export function sendRequestWithHeader(header, method: String, path: String, request: Request, response: Response
    , context: BaseContainer, tag = DEFAULT_TAG) {

    let headers = header || {};
    headers['Accept'] = "application/json";
    headers['Content-Type'] = "application/json";


    let req = {
        method: method,
        headers: headers
    };

    if (request) {
        req['body'] = JSON.stringify(request);
    }

    LOG(context, `sending request --- method: ${method} 
                url: ${path} 
                request: ${JSON.stringify(request)} 
                header -- ${JSON.stringify(headers)}`);

    return fetch(buildRequestURL(path), req)
        .then((res) => {
            LOG(context, `Response coming ---`);
            console.log(res);
            if (res.status !== undefined && res.status >= 200 && res.status < 400) {
                if (res.status === 204)
                    return {};
                else
                    return res.json();
            } else {
                LOG(context, `fail response: ${res.status} ${JSON.stringify(res)}`);
                throw Error('response status is ' + res.status)
            }
        })
        .then(jsonData => {
            LOG(context, "success response: " + JSON.stringify(jsonData));
            context.onSuccess(jsonData, tag);
        })
        .catch((err) => {
            LOG(context, "An error has happened " + err);
            context.onError(err, tag);
        });
}

/**
 * Send request with injected authentication
 * @param header: request header
 * @param method: request method
 * @param path: request path will be appended to base_url
 * @param request: request body
 * @param response: response model
 * @param context: BaseContainer model
 * @param tag: this extra tag will be passed back to onSuccess method of BaseContainer context
 */
export function sendRequestWithHeaderAuth(header, method: String, path: String, request: Request, response: Response
    , context: BaseContainer, tag = DEFAULT_TAG) {
    // let headers = header || {};
    // headers.user_id = "5a0baf55180916352c946b25";
    // headers.access_token = "29d344a166ff88f6480dfee5b0ec4b02ae42e3e7af7725ac8a8365bab3a6db9a";
    // sendRequestWithHeader(headers, method, path, request, response, context, tag);

    getAuthUser().then(authUser => {
        let headers = header || {};
        headers.user_id = authUser._id;
        headers.access_token = authUser.access_token;
        sendRequestWithHeader(headers, method, path, request, response, context, tag);
    })
}