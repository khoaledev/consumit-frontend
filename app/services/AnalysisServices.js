import React from "react";
import {sendRequestWithHeaderAuth} from "./BaseServices";
import BaseContainer from "../containers/BaseContainer";

const USER_BASE_URL = 'analysis/';

function buildUserUrl(url: String = '') {
    return USER_BASE_URL + url;
}

/**
 * Function for getting the food print for the user
 * @param start: start date
 * @param end: end date
 * @param granularity: weekly, monthly or yearly
 * @param scope: user or global
 * @param context: BaseContainer model
 * @param tag: this extra tag will be passed back to onSuccess method of BaseContainer context
 */
export function getFoodPrint(start: Number, end: Number, granularity: string, scope: string,
                             context: BaseContainer, tag) {
    let url = `food_print?start=${start}&end=${end || 0}&granularity=${granularity}&scope=${scope}`;
    url = buildUserUrl(url);
    sendRequestWithHeaderAuth(null, 'GET', url, null, null, context, tag);
}