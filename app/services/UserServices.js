import React from "react";
import {sendRequest, sendRequestWithHeaderAuth} from "./BaseServices";
import BaseContainer from "../containers/BaseContainer";
import {UserProfile} from "../models/UserProfile";
import {PushNotiToken} from "../models/requests/User";
import Login from "../models/requests/Login";
import {LOG} from "../utils/Heplers";

const USER_BASE_URL = 'users/';

function buildUserUrl(url: String = '') {
    return USER_BASE_URL + url;
}

export function signup(login: Login, context: BaseContainer, tag){
    let url = 'signup';
    url = buildUserUrl(url);
    LOG(context, `Signing up with user ${login.email}`);
    sendRequest('POST', url, login, null, context, tag);
}

export function signin(login: Login, context: BaseContainer, tag){
    let url = 'signin';
    url = buildUserUrl(url);
    LOG(context, `Logging in with user ${login.email}`);
    sendRequest('POST', url, login, null, context, tag);
}

export function getUserProfile(userId: String, context: BaseContainer, tag) {
    let url = `${userId}`;
    url = buildUserUrl(url);
    sendRequestWithHeaderAuth(null, 'GET', url, null, null, context, tag);
}

export function getAlexaCode(userId: string, context: BaseContainer, tag) {
    let url = `${userId}/alexa/code`;
    url = buildUserUrl(url);
    sendRequestWithHeaderAuth(null, 'GET', url, null, null, context, tag);

}

export function removeAlexaId(userId: String, context: BaseContainer, tag) {
    // TODO: test remove alexa
    let url = `${userId}/alexa/remove`;
    url = buildUserUrl(url);
    sendRequestWithHeaderAuth(null, 'DELETE', url, null, null, context, tag);
}

export function updateUserProfile(userId: string, profile: UserProfile, context: BaseContainer, tag: string) {
    let url = `${userId}`;
    url = buildUserUrl(url);
    sendRequestWithHeaderAuth(null, 'PUT', url, profile, null, context, tag);
}

export function registerPushToken(userId: String, userToken: PushNotiToken, context: BaseContainer, tag){
    let url = `${userId}/push-tokens`;
    url = buildUserUrl(url);
    LOG(context, `registering push token to the backend`);
    sendRequestWithHeaderAuth(null, 'PUT', url, userToken, null, context, tag);
}

export function removePushToken(userId: String, userToken: PushNotiToken, context: BaseContainer, tag){
    let url = `${userId}/push-tokens`;
    url = buildUserUrl(url);
    LOG(context, `removing push token from the backend`);
    sendRequestWithHeaderAuth(null, 'DELETE', url, userToken, null, context, tag);
}