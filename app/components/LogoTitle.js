import React from "react";
import {Image, View} from "react-native"

export default class LogoTitle extends React.Component {
    render() {
        return (
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Image source={require('../assets/Images/logo_72X72.png')}
                       style={{height: 40, width: 50}} resizeMode={'contain'}/>
                <Image source={require("../assets/Images/consumitname.png")}
                       style={{height: 40, width: 180}} resizeMode={'contain'}/>
            </View>
        )
    }
}