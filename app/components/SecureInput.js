import React, {Component} from "react";
import {TouchableOpacity} from "react-native";
import {Input, Icon, View} from "native-base";

export class SecureInput extends Component {

    constructor(props) {
        super(props);
        this.state = {
            textHide: true,
            withEye: this.props.withEye || true,
        }
    }

    _render_eye() {
        if (this.state.withEye) {
            return (
                <View style={{width: 40, alignItems: 'center'}}>
                    <TouchableOpacity onPressIn={() => this.setState({textHide: false})}
                                      onPressOut={() => this.setState({textHide: true})}>
                        <Icon name={this.state.textHide ? 'ios-eye' : 'ios-eye-off'} size={20}/>
                    </TouchableOpacity>
                </View>
            )
        }
    }

    render() {
        let {value, onChangeText} = this.props;
        return (
            <View style={{flexDirection: 'row'}}>
                <Input secureTextEntry={this.state.textHide} numberOfLines={1} value={value}
                       onChangeText={onChangeText}/>
                {this._render_eye()}
            </View>
        )
    }
}