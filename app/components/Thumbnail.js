import React from "react";
import {Image} from "react-native";

export class Thumbnail extends React.Component {
    render() {
        let size = this.props.size || 50;
        return (
            <Image source={this.props.source}
                   style={{width: size, height: size, margin: 5, borderRadius: size / 2, ...this.props.style}}
            />
        )
    }
}