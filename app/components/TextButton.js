import React, {Component} from "react";
import {Text, TouchableWithoutFeedback, View} from "react-native";
import styles from "../Styles";

export const SIZE_SMALL = "small";
export const SIZE_MEDIUM = "medium";

const BTN_STYLE = "txtBtn_";
const TXT_STYLE = "txtBtnTxt_";

export class TextButton extends Component {

    constructor(props) {
        super(props);
        let btnStyle = [styles.txtBtn_common];
        let txtStyle = [styles.txtBtnTxt_common];
        let size = this.props.size || SIZE_SMALL;
        btnStyle.push(styles[BTN_STYLE + size] || styles[BTN_STYLE + SIZE_SMALL]);
        txtStyle.push(styles[TXT_STYLE + size] || styles[TXT_STYLE + SIZE_SMALL]);
        this.state = {
            btnStyle: btnStyle,
            txtStyle: txtStyle,
        }
    }

    componentDidMount() {
    }

    render() {
        let {onPress, style, children, text} = this.props;
        return (
            <TouchableWithoutFeedback onPress={onPress}>
                <View style={[this.state.btnStyle, style]}>
                    <Text style={this.state.txtStyle}>
                        {children || text}
                    </Text>
                </View>
            </TouchableWithoutFeedback>
        )
    }
}

// export {TextButton};