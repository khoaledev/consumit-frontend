import React, {Component} from "react"
import {View} from "react-native"
import {ItemsListCell} from "./ItemsListCell";
import Swipeout from 'react-native-swipeout';
import {LOG} from "../utils/Heplers";

export class CollapsibleItemListCell extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        // LOG(this, 'collapsible item list');
        return (
            <View>
                <Swipeout left={[{text: 'Edit'}]} right={[{text: 'Edit'}]}>
                    <ItemsListCell item={this.props.item} navigation={this.props.navigation}/>
                </Swipeout>
            </View>
        )
    }
}