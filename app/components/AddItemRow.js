import React from "react";
import {DatePickerAndroid, View, TouchableOpacity, StyleSheet} from "react-native";
import {
    Input,
    Text,
    Icon,
    Toast,
    Textarea
} from "native-base";
import CustomPicker from "react-native-picker";
import Collapsible from 'react-native-collapsible';
import * as Animatable from 'react-native-animatable';
import {PubSubManager} from "../utils/PubSubManager";
import constants from "../utils/Constants";
import FoodItem from "../models/requests/FoodItem";
import {createFoodItem} from "../services/FoodItemsServices";
import BaseContainer from "../containers/BaseContainer";
import {getDateFromDays, getDaysFromDate, initCustomPicker} from "../utils/FoodItem";
import {Thumbnail} from "./Thumbnail";
import {ItemTextButton} from "../components/ItemButton";
import {LOG} from "../utils/Heplers";
import {FoodIcon} from "./TouchableThumbnail";

export class AddItemRow extends BaseContainer {

    constructor(props) {
        super(props);
        let item = this.props.item;
        this._renderCollapsible = this._renderCollapsible.bind(this);
        this._renderHeading = this._renderHeading.bind(this);
        this._unSubscribeAndClose = this._unSubscribeAndClose.bind(this);
        this._subscribeAndOpen = this._subscribeAndOpen.bind(this);
        this._saveFoodItem = this._saveFoodItem.bind(this);
        this._updateItem = this._updateItem.bind(this);
        this._validateData = this._validateData.bind(this);
        this._getHeaderTextColor = this._getHeaderTextColor.bind(this);
        this._initDatePicker = this._initDatePicker.bind(this);

        this.state = {
            id: item.id,
            showToast: false,
            index: this.props.index,
            today: new Date(),
            showDatePicker: false,
            collapsed: true,
            isSaved: false,
            isValid: true,
            isLoading: false,
            item: {
                name: item.name,
                type: item.type,
                date: getDateFromDays(item.days),
                days: item.days,
                quantity: item.quantity || '1',
                quantityType: ['lb'],
                message: '',
            },
        }
    }

    /**
     * date picker for expiration date
     * @private
     */
    _initDatePicker() {
        if (this.state.showDatePicker) {
            DatePickerAndroid.open({date: this.state.item.date, mode: 'spinner', minDate: this.state.today})
                .then(({action, year, month, day}) => {
                    if (action !== DatePickerAndroid.dismissedAction) {
                        let date = new Date(year, month, day);
                        this.setState({
                            item: {
                                ...this.state.item, ...{
                                    date: date,
                                    days: getDaysFromDate(date).toString()
                                }
                            }, showDatePicker: !this.state.showDatePicker
                        });
                        return;
                    }
                    this.setState({showDatePicker: !this.state.showDatePicker});
                })
        }
    }

    componentWillMount() {
        this._subscribeAndOpen();
        PubSubManager.addSubscriber(constants.ADD_ITEM_SAVE, this._saveFoodItem);
    }

    componentWillUnmount() {
        CustomPicker.hide();
        if (this._isOpen())
            this._unSubscribeAndClose();
        PubSubManager.removeSubscriber(constants.ADD_ITEM_SAVE, this._saveFoodItem);
    }

    _validateData() {
        let isValid = true;
        let item = this.state.item;
        if (!item.name || !item.quantityType[0] || isNaN(item.quantity) || isNaN(item.days)) {
            isValid = false;
        }

        if (parseFloat(item.quantity) <= 0 || parseInt(item.days) < 0) {
            isValid = false;
        }

        if (!isValid) {
            let text = 'Please enter all correct details.';
            Toast.show({
                text: text,
                position: 'bottom',
                buttonText: 'Okay',
                type: "danger",
                duration: 3000
            });
        }
        this.setState({isValid: isValid});
        return isValid;
    }

    _saveFoodItem() {
        let isValid = this._validateData();
        if (!isValid) return;
        let item = this.state.item;
        let foodItem = new FoodItem();
        foodItem.name = item.name;
        foodItem.type = item.type;
        foodItem.quantity = parseFloat(item.quantity);
        foodItem.quantity_type = item.quantityType[0];
        foodItem.exp_days = parseInt(item.days);
        foodItem.message = item.message;
        createFoodItem(foodItem, this, 'createItem');
    }

    onSuccess(res, tag) {
        PubSubManager.removeSubscriber(constants.ADD_ITEM_SAVE, this._saveFoodItem);
        this.setState({isSaved: true});
    }

    onError(res, tag) {
        this.setState({isValid: false});
        Toast.show({
            text: "Not able to save the data. Please try again.",
            position: 'bottom',
            buttonText: 'Okay',
            type: "danger",
            duration: 3000
        });
    }

    /**
     * update days state if daysText is a positive integer and within 10 years (3650 days)
     * @param daysText
     * @private
     */
    _onDaysChange = (daysText) => {
        //TODO: parse to Float will be easier
        if (!isNaN(daysText)) {
            this._updateItem({days: daysText})
        }
    };

    _updateItem(updateItem) {
        this.setState({item: {...this.state.item, ...updateItem}});
    }

    _renderRightButtons() {
        if (this.state.isSaved) return null;
        if (this._isClose())
            return <Icon name="ios-arrow-down" style={{color: "#A6A6A6", paddingRight: 10}}/>;
        else
            return <ItemTextButton danger text={'REMOVE'} onPress={() => this.props.deleteItemInState()}/>;
    }

    _isOpen() {
        return !this.state.collapsed;
    }

    _isClose() {
        return this.state.collapsed;
    }

    _unSubscribeAndClose() {
        LOG(this, 'unSubscribe');
        PubSubManager.removeSubscriber(constants.ADD_ITEM, this._unSubscribeAndClose);
        this.setState({collapsed: true});
    }

    _subscribeAndOpen() {
        LOG(this, 'subscribe');
        // force all other to close
        PubSubManager.sendDataToTopic(constants.ADD_ITEM, null);
        PubSubManager.addSubscriber(constants.ADD_ITEM, this._unSubscribeAndClose);
        this.setState({collapsed: false});
    }

    _toggleCollapsed() {
        if (this.state.isSaved) return;
        if (this._isOpen()) {
            this._unSubscribeAndClose();
        } else {
            this.state.isValid = true;
            this._subscribeAndOpen()
        }
    }

    _getHeaderTextColor() {
        let color = '#0E121F';
        if (this.state.isSaved) {
            color = '#02B89A';
        }
        if (!this.state.isValid) {
            color = '#EA4344'
        }
        return color;
    }

    _renderHeading() {
        let item = this.state.item;

        let color = this._getHeaderTextColor();
        return (
            <TouchableOpacity onPress={() => this._toggleCollapsed()}>
                <View style={styles.header}>
                    <View style={{alignItems: 'center', justifyContent: 'center'}}>
                        {/*<Thumbnail source={require('../assets/avatar/default.png')}/>*/}
                        <FoodIcon name={item.name} size={40}/>
                        <View style={{
                            borderRadius: 3, backgroundColor: '#95989A', marginTop: 3,
                            padding: 1, paddingLeft: 3, paddingRight: 3, minWidth: 55,
                        }}>
                            <Text style={{fontSize: 10, color: '#F5F5F5', textAlign: 'center'}}>
                                {item.type || ''}
                            </Text>
                        </View>
                    </View>

                    {/*quantity*/}
                    <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                        <Text style={[styles.headerText, {color: color}]}>{item.quantity || 0} </Text>
                        <Text style={[styles.headerText, {color: color}]}>
                            {item.quantityType[0] === 'count' ? '' : item.quantityType[0]}
                        </Text>
                    </View>

                    {/*name*/}
                    <View style={{justifyContent: 'center', alignItems: 'center'}}>
                        <Text style={[styles.headerText, {color: color}]}>{item.name}</Text>
                    </View>

                    {/*expiration*/}
                    <View style={{justifyContent: 'center', alignItems: 'center'}}>
                        <Text style={[styles.headerText, {color: color}]}>Expiring in {item.days || 0} days</Text>
                    </View>

                    <View style={{justifyContent: 'flex-end', alignItems: 'center'}}>
                        {this._renderRightButtons()}
                    </View>

                </View>
            </TouchableOpacity>
        );
    }

    _renderCollapsible() {
        let item = this.state.item;
        return (
            <Collapsible collapsed={this.state.collapsed} align="center">
                <Animatable.View duration={400} transition="backgroundColor">
                    <Animatable.View animation={this._isOpen() ? 'bounceIn' : undefined}>
                        <View style={styles.content}>
                            <View style={[styles.row]}>
                                <View style={{flex: 1}}>
                                    <Text style={styles.label}>Name</Text>
                                </View>
                                <View style={{flex: 2}}>
                                    <Text style={styles.text}>{item.name}</Text>
                                </View>
                                <View style={{flex: 1}}>
                                </View>

                            </View>
                            <View style={[styles.row]}>
                                <View style={{flex: 1}}>
                                    <Text style={styles.label}>Quantity</Text>
                                </View>
                                <View style={{flex: 1}}>
                                    <Input autoFocus
                                           selectTextOnFocus={true}
                                           numberofLine={1}
                                           onChangeText={text => this._updateItem({quantity: text})}
                                           style={[styles.textbox, {textAlign: 'center'}]}
                                           keyboardType='numeric'
                                           value={item.quantity}
                                    />
                                </View>
                                <View style={{flex: 1, paddingLeft: 15}}>
                                    <Text style={[styles.textbox, {textAlign: 'center'}]}
                                          onPress={() => initCustomPicker({
                                              type: 'Quantity Type',
                                              selectedValue: [item.quantityType[0]],
                                              onPickerConfirm: data => {
                                                  this._updateItem({quantityType: data})
                                              }
                                          })}>
                                        {item.quantityType[0]}
                                        <Icon name="arrow-dropdown" style={{color: "#A6A6A6"}}/>
                                    </Text>
                                </View>
                                <View style={{flex: 1}}>

                                </View>
                            </View>
                            <View style={styles.row}>
                                <View style={{flex: 1.1}}>
                                    <Text style={styles.label}>Expiring in</Text>
                                </View>
                                <View style={{flex: .5}}>
                                    <Input numberofLine={1}
                                           onChangeText={this._onDaysChange}
                                           onEndEditing={() => this._updateItem({date: getDateFromDays(item.days)})}
                                           style={[styles.textbox, {textAlign: 'center'}]}
                                           keyboardType='numeric'
                                           value={item.days}/>
                                </View>
                                <View style={{flex: 1}}>
                                    <Text style={[styles.label, {paddingLeft: 10}]}>days on</Text>
                                </View>
                                <View style={{flex: 1.5}}>
                                    <Text style={[styles.textbox, {textAlign: 'center'}]}
                                          onPress={() => this.setState({showDatePicker: true})}>
                                        {item.date.toLocaleDateString("en-US")}
                                        <Icon name="arrow-dropdown" style={{color: "#A6A6A6"}}/>
                                    </Text>
                                </View>
                            </View>
                            <View style={styles.row}>
                                <Textarea round bordered rowSpan={3}
                                          placeholder='Message'
                                          onChangeText={text => this._updateItem({message: text})}
                                          value={item.message}
                                          style={{textAlign: 'left', flex: 1, borderRadius: 3}}/>
                            </View>
                        </View>
                        {this._initDatePicker()}
                    </Animatable.View>
                </Animatable.View>
            </Collapsible>
        );
    }

    render() {
        return (
            <View style={{
                backgroundColor: 'white', margin: 5, marginTop: 0, marginBottom: 5,
                borderColor: this._isClose() ? '#D8D8D8' : "#5DC9FF", borderRadius: 10, borderWidth: 2
            }}>
                {this._renderHeading()}
                {this._renderCollapsible()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 10,
    },
    headerText: {
        textAlign: 'center',
        fontSize: 14,
        fontWeight: '400',
        color: '#343d5c'
    },
    content: {
        paddingLeft: 25,
        // backgroundColor: '#E8EAF6',
        padding: 10,
        marginBottom: 1,
        // borderBottomWidth: 3,
        // borderBottomColor: '#5C6BC0',
        borderTopWidth: 2,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderTopColor: '#DDD9DF'
    },
    row: {
        paddingTop: 5,
        paddingBottom: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingRight: 20
    },
    label: {
        color: "#575757",
        fontSize: 14,
        paddingRight: 10
    },
    text: {
        fontSize: 14,
    },
    textbox: {
        borderWidth: 1,
        borderColor: "#DDD9DF",
        color: '#0E121F',
        borderRadius: 3,
        height: 40,
        marginBottom: 1,
        marginTop: 1,
        alignItems: 'center',
        justifyContent: 'center',
        fontSize: 14,
    },
    issaved: {
        backgroundColor: '#00BC22',
    },
    iserror: {
        backgroundColor: '#E2252B',
    }
});
