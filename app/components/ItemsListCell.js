import React, {Component} from "react"
import {Text, Icon, Input, Textarea} from "native-base";
import {View, TouchableOpacity, StyleSheet, Alert, TouchableHighlight} from "react-native";
import ProgressBar from "react-native-progress/Bar";
import Collapsible from 'react-native-collapsible';
import {updateFoodItem} from "../services/FoodItemsServices";
import BaseContainer from "../containers/BaseContainer";
import {UpdateFoodItem} from "../models/requests/FoodItem";
import constants from "../utils/Constants";
import {PubSubManager} from "../utils/PubSubManager";
import {FoodIcon} from "./TouchableThumbnail";
import {ItemTextButton} from "./ItemButton";
import {LOG} from "../utils/Heplers";
import TYPE from "../models/requests/FoodItem";

export class ItemsListCell extends BaseContainer {
    constructor(props) {
        super(props);
        this._setUpdateItem = this._setUpdateItem.bind(this);
        this._unSubscribeAndClose = this._unSubscribeAndClose.bind(this);
        let item = this.props.item;
        this.state = {
            collapsed: true,
            item: item,
            updateItem: {
                action: 'eat',
                message: '',
                amount: '1'
            }
        }
    }

    componentDidMount() {
    }

    _setUpdateItem(update) {
        this.setState({updateItem: {...this.state.updateItem, ...update}});
    }

    _updateFoodQuantity() {
        let {action, amount, message} = this.state.updateItem;
        let {quantity} = this.state.item;
        if (isNaN(amount)) {
            Alert.alert(
                '',
                'Enter correct quantity',
                [
                    {text: 'Okay'}
                ]);
            return;
        }
        else if (parseFloat(amount) <= 0) {
            Alert.alert(
                '',
                'Enter correct quantity',
                [
                    {text: 'Okay'}
                ]);
            return;
        }
        if (parseFloat(amount) > quantity) {
            Alert.alert(
                '',
                'Entered quantity cannot be greater than total quantity',
                [
                    {text: 'Okay'}
                ]);
            return;
        }
        updateFoodItem(this.state.item._id, new UpdateFoodItem(action, amount, message), this, 'updateQuantity')
    }

    onSuccess(res, tag) {
        if (tag === 'updateQuantity') {
            this.setState({item: res, collapsed: true});
            if (this.props.updateItem)
                this.props.updateItem({index: this.props.index, item: res, section: this.props.section});
        }
    }

    onError(res, tag) {
        if (tag === 'updateQuantity') {
            Alert.alert(
                '',
                'Not able to update the inventory, please try again.',
                [
                    {text: 'Okay'}
                ]);
        }
    }

    _render_status_bars() {
        let item = this.state.item;
        let daysLeft = item.days_left;
        let expired = item.is_expired;
        let expiryColor = '#29C778';
        if (daysLeft < 2)
            expiryColor = '#ECB228';
        if (daysLeft < 0)
            expiryColor = '#EA4344';
        let days_status = expired ?
            `expired ${-daysLeft} days ago` :
            `${daysLeft} days left`;

        if(daysLeft === 0){
            days_status = expired ?
                `expired today` :
                `expiring today`;
        }

        let quantity_status = expired ?
            `wasted ${item.quantity} ${item.quantity_type.includes('count') ? '' : item.quantity_type}` :
            `${item.quantity}${item.quantity_type.includes('count') ? '' : ' ' + item.quantity_type} left`;
        return (
            <View>
                <FoodQuantityBar quantity={item.quantity}
                                 purchase_quantity={item.consumption[0].quantity}
                                 color={expiryColor}/>
                <FoodExpirationBar expDays={item.exp_days}
                                   remainDays={daysLeft}/>
                <View style={{flexDirection: 'row'}}>
                    <Text style={{fontSize: 14, color: expiryColor}}>
                        {`${quantity_status}`}
                    </Text>
                    <Text style={{fontSize: 14, color: '#5C5C5C'}}>
                        {` / ${days_status}`}
                    </Text>
                </View>
            </View>
        )
    }

    _toggleCollapsed() {
        if (!this._shouldToggle())
            return;

        if (this._isOpen()) {
            this._unSubscribeAndClose();
        } else {
            LOG(this, `Open FoodItem with Id ${this.state.item._id} for editting`);
            this._sendCloseAllRequest();
            this._subscribeAndOpen();
        }
    }

    _shouldToggle = () => !(this.state.item.is_expired || this.state.item.is_consumed);

    _isOpen = () => !this.state.collapsed;

    // _isClose = () => this.state.collapsed;

    _open = () => this.setState({collapsed: false});

    _close = () => this.setState({collapsed: true});

    _sendCloseAllRequest = () => PubSubManager.sendDataToTopic(constants.INVENTORY_ITEM_COLLAPSE, null);

    _subscribeAndOpen() {
        PubSubManager.addSubscriber(constants.INVENTORY_ITEM_COLLAPSE, this._unSubscribeAndClose);
        this._open();
    }

    _unSubscribeAndClose() {
        PubSubManager.removeSubscriber(constants.INVENTORY_ITEM_COLLAPSE, this._unSubscribeAndClose);
        this._close();
    }

    _renderLeftBut() {
        if (!this._shouldToggle())
            return null;

        if (this._isOpen()) {
            return (
                <View>
                    <ItemTextButton danger text={'CANCEL'} onPress={this._toggleCollapsed.bind(this)}/>
                    <ItemTextButton text={'SAVE'} onPress={this._updateFoodQuantity.bind(this)}/>
                </View>)
        }
        return <Icon name="ios-arrow-down-outline" style={{color: '#D8D8D8', paddingRight: 10}}
                     onPress={() => this._toggleCollapsed()}/>
    }

    _renderHeader() {
        let item = this.state.item;
        let expired = item.is_expired;
        return (
            <TouchableOpacity
                style={{
                    flex: 1,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    padding: 10,
                }}
                onPress={expired ? null : () => this._toggleCollapsed()}>

                {/*Thumbnail and food cat button*/}
                <View style={{alignItems: 'center', justifyContent: 'center'}}>
                    <FoodIcon name={item.name} size={40} onPress={() => this._filterFood({name: item.name})}/>
                    <FoodTypeButton text={item.type}/>
                </View>
                {/*middle part, name, status bars, and text*/}
                <View style={{
                    flex: 1,
                    justifyContent: 'flex-start',
                    alignItems: 'stretch',
                    paddingRight: 10,
                    paddingLeft: 10
                }}>
                    <Text style={{fontSize: 14, color: '#343d5c', textAlign: 'left'}}>{item.name}</Text>
                    {this._render_status_bars(item)}
                </View>

                {/*left button*/}
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                    {this._renderLeftBut()}
                </View>
            </TouchableOpacity>
        )
    }

    _filterFood(filter) {
        if (this.props.filterFood)
            this.props.filterFood(filter);
    }

    render() {
        let item = this.state.item;
        return (
            <View style={{
                backgroundColor: 'white', margin: 5, marginBottom: 0, borderRadius: 5, borderWidth: 2,
                borderColor: this.state.collapsed ? '#D8D8D8' : "#5DC9FF",
            }}>
                {/*header*/}
                {this._renderHeader()}

                {/*update fields*/}
                {/*<CollapsibleFoodItemUpdate*/}
                {/*collapsed={this.state.collapsed}*/}
                {/*item={item}*/}
                {/*updateItem={this.state.updateItem}*/}
                {/*setUpdateItem={this._setUpdateItem}*/}
                {/*/>*/}
                <CollapsibleFoodQuantityUpdate
                    collapsed={this.state.collapsed}
                    item={item}
                    updateItem={this.state.updateItem}
                    setUpdateItem={this._setUpdateItem}
                />
            </View>
        )
    }
}

export class FoodTypeButton extends Component {
    render() {
        return (
            <View style={{marginTop: 3}}>
                <View style={{
                    borderRadius: 3, backgroundColor: '#95989A', minWidth: 55,
                    padding: 1, paddingLeft: 3, paddingRight: 3
                }}>
                    <Text style={{fontSize: 10, color: '#F5F5F5', textAlign: 'center'}}>
                        {this.props.text || 'custom'}
                    </Text>
                </View>
            </View>
        )
    }
}

export class FoodQuantityBar extends Component {
    render() {
        let progress = this.props.quantity / (this.props.purchase_quantity || 1);
        let color = this.props.color;
        return (
            <View style={{flex: 1}}>
                <ProgressBar progress={progress} color={color} borderColor={color}
                             unfilledColor={'#D8D8D8'}
                             borderRadius={3}
                             borderWidth={1} height={6}
                             width={null}/>
            </View>
        )
    }
}

export class FoodExpirationBar extends Component {
    render() {
        let expDays = this.props.expDays || 1;
        let remainDays = this.props.remainDays || 0;
        remainDays = Math.max(remainDays, 0);
        let progress = remainDays / expDays;
        return (
            <View style={{flex: 1}}>
                <ProgressBar progress={progress}
                             color={'#5C5C5C'} unfilledColor={'#D8D8D8'}
                             borderRadius={2}
                             borderWidth={0} borderColor={'white'} height={4}
                             width={null}/>
            </View>
        )
    }
}

export class CollapsibleFoodItemUpdate extends Component {
    render() {
        let collapsed = this.props.collapsed;
        return (
            <View>
                <Collapsible collapsed={collapsed === undefined ? true : collapsed} align={'center'}>
                    <View style={styles.content}>
                        <View style={styles.row}>
                            <Input disabled placeholder='Eat'
                                   style={{textAlign: 'left', flex: 1}}
                                   value={this.props.updateItem.action}/>
                            <Input numberOfLine={1}
                                   placeholder='Quantity'
                                   onChangeText={text => this.props.setUpdateItem({amount: text})}
                                   style={{textAlign: 'left', flex: 1}}
                                   keyboardType='numeric'
                                   value={this.props.updateItem.amount}/>
                            <Input disabled
                                   style={{textAlign: 'left', flex: 1}}
                                   value={this.props.item.quantity_type.includes('count') ? '' : this.props.item.quantity_type}/>
                        </View>
                        <View style={styles.row}>
                            <Textarea round bordered rowSpan={3}
                                      placeholder='Message'
                                      onChangeText={text => this.props.setUpdateItem({message: text})}
                                      value={this.props.updateItem.message}
                                      style={{textAlign: 'left', flex: 1, borderRadius: 3}}/>
                        </View>
                    </View>
                </Collapsible>
            </View>
        )
    }
}

export class CollapsibleFoodQuantityUpdate extends Component {

    constructor() {
        super();
        this.state = {
            number: 1.0,
        };
        this.timer = null;
        this.addOne = this.addOne.bind(this);
        this.subOne = this.subOne.bind(this);
        this.stopTimer = this.stopTimer.bind(this);
    }

    componentWillMount() {
        if (this.props.item.quantity_type.includes('count')) {
            this.step = 1;
        }
        else {
            this.step = .5;
        }
        if(this.state.number > this.props.item.quantity){
            this.state.number = this.props.item.quantity;
        }
    }

    addOne() {
        if (this.state.number >= this.props.item.quantity)
            return;
        this.setState({number: this.state.number + this.step}, () => this.props.setUpdateItem({amount: this.state.number.toFixed(1)}));
        this.timer = setTimeout(this.addOne, 200);
    }

    subOne() {
        if (this.state.number.toFixed(1) <= this.step)
            return;
        this.setState({number: this.state.number - this.step}, () => this.props.setUpdateItem({amount: this.state.number.toFixed(1)}));
        this.timer = setTimeout(this.subOne, 200);
    }

    stopTimer() {
        clearTimeout(this.timer);
    }

    render() {
        let collapsed = this.props.collapsed;
        return (
            <View>
                <Collapsible collapsed={collapsed === undefined ? true : collapsed} align={'center'}>
                    <View style={styles.content}>
                        <View style={[styles.row, {justifyContent: 'space-around'}]}>
                            <Text style={{fontSize: 15, color: '#343d5c'}}>{this.props.updateItem.action}</Text>
                            <View style={[styles.row, {justifyContent: 'center'}]}>
                                <TouchableHighlight
                                    style={{
                                        borderBottomLeftRadius: 5,
                                        borderTopLeftRadius: 5,
                                        backgroundColor: '#95989A',
                                        minWidth: 30,
                                        height: 35
                                    }}
                                    onPressIn={this.subOne} onPressOut={this.stopTimer}>
                                    <Text style={{color: '#FFF', fontSize: 25, textAlign: 'center'}}>-</Text>
                                </TouchableHighlight>
                                <Text style={{
                                    fontSize: 15,
                                    color: '#343d5c',
                                    borderTopWidth: 2,
                                    borderBottomWidth: 2,
                                    borderColor: '#95989A',
                                    textAlign: 'center',
                                    minWidth: 100,
                                    height: 35,
                                    alignItems: 'center',
                                }}>
                                    {this.props.item.quantity_type.includes('count') ? this.state.number.toFixed(0) : this.state.number.toFixed(1)}
                                </Text>
                                <TouchableHighlight
                                    style={{
                                        borderBottomRightRadius: 5,
                                        borderTopRightRadius: 5,
                                        backgroundColor: '#95989A',
                                        minWidth: 30,
                                        height: 35
                                    }}
                                    onPressIn={this.addOne} onPressOut={this.stopTimer}>
                                    <Text style={{color: '#FFF', fontSize: 25, textAlign: 'center'}}>+</Text>
                                </TouchableHighlight>
                            </View>
                            <Text>
                                {` ${this.props.item.quantity_type.includes('count')
                                    ? '' : this.props.item.quantity_type} `}
                            </Text>
                        </View>
                        {/*<View style={[styles.row, {justifyContent: 'center'}]}>*/}
                        {/*<Text style={{fontSize: 15, color: '#343d5c'}}>Select Quantity</Text>*/}
                        {/*</View>*/}
                        {/*<View style={[styles.row, {justifyContent: 'space-around'}]}>*/}
                        {/*<TouchableOpacity onPressIn={this.subOne} onPressOut={this.stopTimer}>*/}
                        {/*<Icon name="remove-circle" style={{color: '#FF7658', fontSize: 40}}/>*/}
                        {/*</TouchableOpacity>*/}
                        {/*<Text style={{fontSize: 16, color: '#343d5c'}}>*/}
                        {/*{` ${this.state.number}`}*/}
                        {/*{` ${this.props.item.quantity_type === 'count' ? '' : this.props.item.quantity_type} `}*/}
                        {/*</Text>*/}
                        {/*<TouchableOpacity onPressIn={this.addOne} onPressOut={this.stopTimer}>*/}
                        {/*<Icon name="add-circle" style={{color: '#29C778', fontSize: 40}}/>*/}
                        {/*</TouchableOpacity>*/}
                        {/*</View>*/}
                    </View>
                </Collapsible>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        flex: 1,
        flexDirection: 'row',
        marginBottom: 1,
        justifyContent: 'flex-start',
        paddingLeft: 20,
        paddingRight: 10,
        paddingTop: 10,
        paddingBottom: 10,
    },
    headerText: {
        textAlign: 'center',
        color: '#FFFFFF',
        fontSize: 14,
        fontWeight: '400',
    },
    content: {
        paddingLeft: 25,
        // backgroundColor: '#E8EAF6',
        padding: 10,
        marginBottom: 1,
        // borderBottomWidth: 3,
        // borderBottomColor: '#5C6BC0',
        borderTopWidth: 2,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderTopColor: '#D8D8D8'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    active: {
        backgroundColor: '#5C6BC0',
    },
    inactive: {
        backgroundColor: '#7986CB',
    }
});