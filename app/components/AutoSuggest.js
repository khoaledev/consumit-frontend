import React, {Component} from "react";
import {View, TouchableOpacity, StyleSheet, Text, Keyboard} from "react-native";
import {Input, Item, Icon} from "native-base";
import Autocomplete from "react-native-autocomplete-input";
import {Thumbnail} from "./Thumbnail";
import {FoodIcon} from "./TouchableThumbnail";

export default class AutoSuggest extends Component {

    constructor(props) {
        super(props);
        this._onSuggestionClick = this._onSuggestionClick.bind(this);
        this._renderTextInput = this._renderTextInput.bind(this);
        this._renderSuggestion = this._renderSuggestion.bind(this);
        this.state = {
            items: this.props.suggestions,
            filter_items: [],
            place_holder: this.props.place_holder,
            previous_value: this.props.selected_value || '',
            selected_value: this.props.selected_value || '',
            inputFocused: false,
        };
    }

    _suggestedValues(value) {
        let items = this.state.items;
        if(items.length <= 0)
            return;
        let previous_value = this.state.selected_value;
        let filter_items = [];
        if (value) {
            /**
             * to improve performance we check if previous value is ''.
             * If it is, we filter from the full list
             * If it not, we check if there is a new character (by length)
             *      If there is a new character, we take the last filter items and continue filter from that
             *      If not (a deletion), we just ignore and use the full list
             */
            let previous_filter_items = items;
            if (previous_value && value.length > previous_value.length)
                previous_filter_items = this.state.filter_items;
            filter_items = previous_filter_items.filter(item => item.toLowerCase().includes(value.toLowerCase()));
        }
        this.setState({filter_items});
    }

    _onTextChange(value) {
        this._suggestedValues(value);
        this.setState({selected_value: value});
    }

    _onSuggestionClick(value) {
        this.props.onSelectSuggestion(value);
        if(this.props.resetInputOnSelect)
            this.setState({selected_value: '', filter_items: []});
        else
            this.setState({selected_value: value, filter_items: []});
    }

    _onSubmitEditing() {
        if (this.state.filter_items.length > 0)
            this._onSuggestionClick(this.state.filter_items[0])
    }

    _renderSuggestion(value, index) {
        let backgroundColor = (index % 2) === 0 ? '#EFEFEF' : 'white';
        let textColor = parseInt(index) === 0 ? '#5DC9FF' : 'black';
        return (
            <TouchableOpacity onPress={() => this._onSuggestionClick(value)}
                              style={{
                                  backgroundColor,
                                  height: 40, flexDirection: 'row', alignItems: 'center'
                              }}>
                <FoodIcon name={value} size={32}/>
                <Text style={{fontSize: 14, color: textColor, paddingLeft: 10}}>
                    {value}
                </Text>
            </TouchableOpacity>
        )
    }

    _renderTextInput(props) {
        return (
            <Item rounded style={{backgroundColor: this.state.inputFocused ? '#D8D8D8' : '#F5F5F5'}}>
                <Input {...props} rounded style={{height: 40, fontSize: 14,}}
                       selectTextOnFocus={true}
                       blurOnSubmit={true}
                       onFocus={() => this.setState({inputFocused: true})}
                       onBlur={() => this.setState({inputFocused: false})}
                       onSubmitEditing={this._onSubmitEditing.bind(this)}
                />
            </Item>
        )
    }

    render() {
        return (
            <View style={styles.autocompleteContainer}>
                <Autocomplete
                    listStyle={{maxHeight: 250}}
                    data={this.state.filter_items}
                    defaultValue={this.state.selected_value || ''}
                    onChangeText={value => this._onTextChange(value)}
                    hideResults={!this.state.inputFocused}
                    placeholder={this.state.place_holder}
                    renderItem={(value, sec, id) => this._renderSuggestion(value, id)}
                    inputContainerStyle={{padding: 5, backgroundColor: 'white'}}
                    renderTextInput={this._renderTextInput}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    autocompleteContainer: {
        flex: 1,
        left: 0,
        position: 'absolute',
        right: 0,
        top: 0,
        zIndex: 999,
        borderColor: 'white'
    },
    itemText: {
        fontSize: 14,
        color: '#000000',
    }
});