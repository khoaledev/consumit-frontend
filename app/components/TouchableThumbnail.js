import React from "react";
import {Image, TouchableOpacity} from "react-native";

export class TouchableThumbnail extends React.Component {
    render() {
        let size = this.props.size || 50;
        return (
            <TouchableOpacity
                onPress={() => {
                    if (this.props.onPress) this.props.onPress()
                }}>
                <Image source={this.props.source}
                       resizeMode={'contain'}
                       style={{width: size, height: size, borderRadius: size / 2, marginLeft:5, marginRight: 5}}
                       onPress={this.props.onPress}/>
            </TouchableOpacity>
        )
    }
}

export class FoodIcon extends React.Component {
    getImage(name) {
        let source = require('../assets/Images/consumit.png');
        switch (name) {
            case "apple":
                source = require('../assets/icons/apple.png');
                break;
            case "avocado":
                source = require('../assets/icons/avocado.png');
                break;
            case "banana":
                source = require('../assets/icons/banana.png');
                break;
            case "grapes":
                source = require('../assets/icons/grapes.png');
                break;
            case "guava":
                source = require('../assets/icons/guava.png');
                break;
            case "mango":
                source = require('../assets/icons/mango.png');
                break;
            case "orange":
                source = require('../assets/icons/orange.png');
                break;
            case "papaya":
                source = require('../assets/icons/papaya.png');
                break;
            case "pear":
                source = require('../assets/icons/pear.png');
                break;
            case "watermelon":
                source = require('../assets/icons/watermelon.png');
                break;
            case "asparagus":
                source = require('../assets/icons/asparagus.png');
                break;
            case "beet":
                source = require('../assets/icons/beet.png');
                break;
            case "broccoli":
                source = require('../assets/icons/broccoli.png');
                break;
            case "cabbage":
                source = require('../assets/icons/cabbage.png');
                break;
            case "carrot":
                source = require('../assets/icons/carrot.png');
                break;
            case "garlic":
                source = require('../assets/icons/garlic.png');
                break;
            case "mushroom":
                source = require('../assets/icons/mushrooms.png');
                break;
            case "onion":
                source = require('../assets/icons/onion.png');
                break;
            case "pepper":
                source = require('../assets/icons/pepper.png');
                break;
            case "potato":
                source = require('../assets/icons/potatoes.png');
                break;
            case "tomato":
                source = require('../assets/icons/tomato.png');
                break;
            case "zucchini":
                source = require('../assets/icons/zucchini.png');
                break;
            case "blackberries":
                source = require('../assets/icons/blackberry.png');
                break;
            case "blueberries":
                source = require('../assets/icons/blueberry.png');
                break;
            case "grapefruit":
                source = require('../assets/icons/grapefruit.png');
                break;
            case "lemon":
                source = require('../assets/icons/lemon.png');
                break;
            case "lime":
                source = require('../assets/icons/lime.png');
                break;
            case "nectarine":
                source = require('../assets/icons/nectarine.png');
                break;
            case "peach":
                source = require('../assets/icons/peach.png');
                break;
            case "pineapple":
                source = require('../assets/icons/pineapple.png');
                break;
            case "plum":
                source = require('../assets/icons/plum.png');
                break;
            case "raspberries":
                source = require('../assets/icons/raspberry.png');
                break;
            case "strawberries":
                source = require('../assets/icons/strawberry.png');
                break;
            case "bell pepper":
                source = require('../assets/icons/bell-pepper.png');
                break;
            case "cauliflower":
                source = require('../assets/icons/cauliflower.png');
                break;
            case "celery":
                source = require('../assets/icons/celery.png');
                break;
            case "cucumber":
                source = require('../assets/icons/cucumber.png');
                break;
            case "green bean":
                source = require('../assets/icons/green-beans.png');
                break;
            case "green onion":
                source = require('../assets/icons/green-onion.png');
                break;
            case "lettuce":
                source = require('../assets/icons/lettuce.png');
                break;
            case "spinach":
                source = require('../assets/icons/spinach.png');
                break;
            case "sweet potato":
                source = require('../assets/icons/sweet-potato.png');
                break;
            case "butter":
                source = require('../assets/icons/butter.png');
                break;
            case "cheese":
                source = require('../assets/icons/cheese.png');
                break;
            case "cream":
                source = require('../assets/icons/cream.png');
                break;
            case "ice cream":
                source = require('../assets/icons/ice-cream.png');
                break;
            case "margarine":
                source = require('../assets/icons/margarine.png');
                break;
            case "milk":
                source = require('../assets/icons/milk.png');
                break;
            case "whipped cream":
                source = require('../assets/icons/whipped-cream.png');
                break;
            case "yogurt":
                source = require('../assets/icons/yogurt.png');
                break;
            case "bacon":
                source = require('../assets/icons/bacon.png');
                break;
            case "beef":
                source = require('../assets/icons/beef.png');
                break;
            case "chicken":
                source = require('../assets/icons/chicken.png');
                break;
            case "eggs":
                source = require('../assets/icons/egg.png');
                break;
            case "lamb":
                source = require('../assets/icons/lamb.png');
                break;
            case "pork":
                source = require('../assets/icons/pork.png');
                break;
            case "salami":
                source = require('../assets/icons/salami.png');
                break;
            case "shrimp":
                source = require('../assets/icons/shrimp.png');
                break;
            case "sausage":
                source = require('../assets/icons/sausage.png');
                break;
            case "turkey":
                source = require('../assets/icons/turkey.png');
                break;
            case "barley":
                source = require('../assets/icons/barley.png');
                break;
            case "maize":
                source = require('../assets/icons/maize.png');
                break;
            case "oat":
                source = require('../assets/icons/oat.png');
                break;
            case "wheat":
                source = require('../assets/icons/wheat.png');
                break;
            case "rice":
                source = require('../assets/icons/rice.png');
                break;
            case "beans":
                source = require('../assets/icons/beans.png');
                break;
            case "peas":
                source = require('../assets/icons/peas.png');
                break;
            case "soybean":
                source = require('../assets/icons/soybean.png');
                break;
            default:
                source = require('../assets/Images/consumit.png');
                break;
        }
    return <TouchableThumbnail {...this.props} source={source}/>
    }

    render() {
        let name = this.props.name;
        return this.getImage(name.toLowerCase());
    }
}