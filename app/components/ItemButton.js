import React, {Component} from "react";
import {Text, TouchableWithoutFeedback, View, TouchableOpacity} from "react-native";

export class ItemTextButton extends Component {

    constructor(props) {
        super(props);
        this.state = {
            defaultBtnStyle: {
                backgroundColor: '#5DC9FF',
                borderRadius: 5,
                margin: 3,
                padding: 3,
                paddingTop: 10,
                paddingBottom: 10,
                justifyContent: 'center',
                alignItems: 'center',
            },
            defaultTextStyle: {color: 'white', fontSize: 10,},
        }
    }

    _getButtonStyle() {
        let style = {...this.state.defaultBtnStyle};
        if (this.props.danger)
            style.backgroundColor = '#FF7658';
        if (this.props.transparent) {
            delete style.backgroundColor;
            style.borderWidth = 1;
            style.borderColor = 'white';
        }
        return style;
    }

    _getTextStyle() {
        return {...this.state.defaultTextStyle};
    }

    _renderText() {
        let {text, textStyle} = this.props;
        return (
            <Text style={{...this._getTextStyle(), ...textStyle}}>
                {text}
            </Text>)
    }

    render() {
        let {onPress, buttonStyle, children} = this.props;
        return (
            <TouchableOpacity onPress={onPress}>
                <View style={{...this._getButtonStyle(), ...buttonStyle}}>
                    {children || this._renderText()}
                </View>
            </TouchableOpacity>
        )
    }
}

// export {TextButton};