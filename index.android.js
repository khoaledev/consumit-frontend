/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from "react";
import {AppRegistry} from "react-native";
import {StackNavigator, TabNavigator, SwitchNavigator } from "react-navigation";
import HomeScreen from "./app/containers/HomeScreen";
import {AddItemScreen} from "./app/containers/AddItemScreen";
import LandingScreen from "./app/containers/LandingScreen";
import SignupScreen from "./app/containers/SignupScreen";
import SigninScreen from "./app/containers/SigninScreen"

const AuthNavigation = StackNavigator({
    Landing: {screen: LandingScreen},
    SignUp: {screen: SignupScreen},
    SignIn: {screen: SigninScreen},
},{
    mode: 'screen',
    initialRouteName: 'Landing',
});

const AppNavigation = StackNavigator({
    Home: {screen: HomeScreen},
    AddItem: {screen: AddItemScreen},
},{
    mode: 'screen',
    initialRouteName: 'Home'
});

const ConsumitNavigation = SwitchNavigator({
    Landing: AuthNavigation,
    Home: AppNavigation
}, {
    mode: 'screen',
    initialRouteName: 'Landing'
});



const devNav = StackNavigator({
    DevScreen: {screen: AddItemScreen},
    // headerRight: {screen: FriendsScreen}
});

AppRegistry.registerComponent('consumit', () => ConsumitNavigation);
// AppRegistry.registerComponent('consumit', () => devNav);

